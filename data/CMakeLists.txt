# This is the directory where all the resource files should be installed.
set(RESOURCES_DIR share/monochromeworlds)

# Process subdirectories.
add_subdirectory(gfx)
add_subdirectory(music)
add_subdirectory(sfx)
add_subdirectory(stages)
