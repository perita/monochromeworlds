##
# Sets the version's variables.
#
# This will set the following CMake variables:
#
#   - VERSION_MAJOR: The major parameter.
#   - VERSION_MINOR: The minor parameter.
#   - VERSION_PATCH: The patch parameter.
#   - VERSION_REV: The working directory's changeset id.
#   - VERSION: Either major.minor.patch or major.minor.patch.rev,
#              depending on the platform.
#
macro(SET_VERSION _major _minor _patch)
    # No longer using Mercurial, thus no revision version.
    set(VERSION_REV "0")

    # This is the application's version.
    set(VERSION_MAJOR ${_major})
    set(VERSION_MINOR ${_minor})
    set(VERSION_PATCH ${_patch})
    set(VERSION "${VERSION_MAJOR}.${VERSION_MINOR}.${VERSION_PATCH}")
    # On non-Apple builds, append the revision as the last component.
    # On Apple builds, we'll know it from the bundle version.
    if(NOT APPLE)
        set(VERSION "${VERSION}.${VERSION_REV}")
    endif(NOT APPLE)
endmacro(SET_VERSION)
