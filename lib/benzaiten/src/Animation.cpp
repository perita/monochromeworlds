//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // !HAVE_BENZAITEN_CONFIG_H
#include "Animation.hpp"

using namespace benzaiten;

Animation::Animation (const SpriteSheet &spriteSheet, unsigned int startIndex,
        unsigned int count, unsigned int speed, bool loop, Direction direction):
    begin_ (startIndex),
    currentFrame_ (startIndex),
    ended_ (false),
    direction_ (direction),
    end_ (startIndex + count * direction),
    loop_ (loop),
    speed_ (speed),
    spriteSheet_ (spriteSheet),
    time_ (speed)
{
}

SDL_Rect
Animation::draw (int16_t x, int16_t y, Surface &screen) const
{
    return spriteSheet().draw (x, y, currentFrame_, screen);
}

void
Animation::firstFrame ()
{
    currentFrame_ = begin_;
    time_ = speed_;
    ended_ = false;
}

bool
Animation::hasEnded () const
{
    return ended_;
}

size_t
Animation::height () const
{
    return spriteSheet ().height ();
}

void
Animation::lastFrame ()
{
    currentFrame_ = end_ - direction_;
    time_ = speed_;
    ended_ = !loop_;
}

void
Animation::nextFrame ()
{
    if ( 0 == time_ )
    {
        time_ = speed_;
        currentFrame_ += direction_;
        if ( end_ == currentFrame_ )
        {
            if ( loop_ )
            {
                currentFrame_ = begin_;
            }
            else
            {
                currentFrame_ = end_ - direction_;
                ended_ = true;
            }
        }
    }
    else
    {
        --time_;
    }
}

void
Animation::setAlpha (uint8_t alpha)
{
    spriteSheet().setAlpha (alpha);
}

SpriteSheet &
Animation::spriteSheet ()
{
    return spriteSheet_;
}

const SpriteSheet &
Animation::spriteSheet () const
{
    return static_cast<const SpriteSheet &> (
            const_cast<Animation &> (*this).spriteSheet());
}

size_t
Animation::width () const
{
    return spriteSheet ().width ();
}
