//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // !HAVE_BENZAITEN_CONFIG_H
#include "EventManager.hpp"

using namespace benzaiten;

#define SDL_X_AXIS 0
#define SDL_Y_AXIS 1
#define DEAD_ZONE 10000

EventManager::EventManager ():
    clickPressed_ (false),
    joyAxisMap_(),
    joyMap_ (),
    joystick_ (SDL_JoystickOpen (0)),
    joyXAxis_(AxisNone),
    joyYAxis_(AxisNone),
    keyMap_ ()
{
}

EventManager::~EventManager ()
{
    /* XXX: Somehow it makes the application crash in GP2X.
    if ( SDL_JoystickOpened (0) )
    {
        SDL_JoystickClose (joystick_);
    }
    */
}

void
EventManager::mapJoy (uint8_t button, int action)
{
    joyMap_[button] = action;
}

void
EventManager::mapJoyAxis(Axis axis, int action)
{
    joyAxisMap_[axis] = action;
}

void
EventManager::mapKey (SDLKey key, int action)
{
    keyMap_[key] = action;
}

void
EventManager::setXAxis(Axis axis)
{
    if (joyXAxis_ != axis)
    {
        if (AxisNone != joyXAxis_)
        {
            JoyAxisMap::iterator action (joyAxisMap_.find (joyXAxis_));
            if (action != joyAxisMap_.end())
            {
                OnActionEnd(action->second);
            }
        }
        if (AxisNone != axis)
        {
            if (AxisLeft == axis)
            {
                OnMenuAction(Left);
            }
            else
            {
                OnMenuAction(Right);
            }
            JoyAxisMap::iterator action (joyAxisMap_.find (axis));
            if (action != joyAxisMap_.end())
            {
                OnActionBegin(action->second);
            }
        }
        joyXAxis_ = axis;
    }
}

void
EventManager::setYAxis(Axis axis)
{
    if (joyYAxis_ != axis)
    {
        if (AxisNone != joyYAxis_)
        {
            JoyAxisMap::iterator action (joyAxisMap_.find (joyYAxis_));
            if (action != joyAxisMap_.end())
            {
                OnActionEnd(action->second);
            }
        }
        if (AxisNone != axis)
        {
            if (AxisDown == axis)
            {
                OnMenuAction(Down);
            }
            else
            {
                OnMenuAction(Up);
            }
            JoyAxisMap::iterator action (joyAxisMap_.find (axis));
            if (action != joyAxisMap_.end())
            {
                OnActionBegin(action->second);
            }
        }
        joyYAxis_ = axis;
    }
}
void
EventManager::update ()
{
    SDL_Event event;
    while ( SDL_PollEvent (&event) )
    {
        switch ( event.type )
        {
            case SDL_QUIT:
                OnQuit ();
                break;

            case SDL_ACTIVEEVENT:
                if (event.active.state & SDL_APPINPUTFOCUS &&
                        0 == event.active.gain)
                {
                    OnLostFocus();
                }

            case SDL_KEYDOWN:
                switch ( event.key.keysym.sym )
                {
                    case SDLK_ESCAPE:
                        OnMenuAction (Cancel);
                        break;

                    case SDLK_UP:
                        OnMenuAction (Up);
                        break;

                    case SDLK_DOWN:
                        OnMenuAction (Down);
                        break;

                    case SDLK_RETURN:
                        OnMenuAction (Select);
                        break;

                    case SDLK_LEFT:
                        OnMenuAction (Left);
                        break;

                    case SDLK_RIGHT:
                        OnMenuAction (Right);
                        break;

                    case SDLK_z:
                        OnMenuAction (Select);
                        break;

                    default:
                        // Ignore.
                        break;
                }

                {
                    KeyMap::iterator key (keyMap_.find (event.key.keysym.sym));
                    if ( key != keyMap_.end () )
                    {
                        OnActionBegin (key->second);
                    }
                }
                break;

            case SDL_KEYUP:
                {
                    KeyMap::iterator key (keyMap_.find (event.key.keysym.sym));
                    if ( key != keyMap_.end () )
                    {
                        OnActionEnd (key->second);
                    }
                }
                break;

            case SDL_JOYBUTTONDOWN:
                switch (event.jbutton.button)
                {
#if defined (GP2X)
                    case GP2X_BUTTON_A:
                        OnMenuAction (Select);
                        break;

                    case GP2X_BUTTON_B:
                        OnMenuAction (Select);
                        break;

                    case GP2X_BUTTON_CLICK:
                        clickPressed_ = true;
                        break;

                    case GP2X_BUTTON_DOWN:
                        OnMenuAction (Down);
                        break;

                    case GP2X_BUTTON_LEFT:
                        OnMenuAction (Left);
                        break;

                    case GP2X_BUTTON_RIGHT:
                        OnMenuAction (Right);
                        break;

                    case GP2X_BUTTON_START:
                        if (clickPressed_)
                        {
                            OnQuit ();
                        }
                        break;

                    case GP2X_BUTTON_UP:
                        OnMenuAction (Up);
                        break;

                    case GP2X_BUTTON_X:
                        OnMenuAction (Cancel);
                        break;
#else // !GP2X
                    case 0:
                    case 7:
                        OnMenuAction(Select);
                        break;

                    case 3:
                        OnMenuAction(Cancel);
                        break;
#endif // GP2X
                }
                {
                    JoyMap::iterator button (
                            joyMap_.find (event.jbutton.button));
                    if ( button != joyMap_.end () )
                    {
                        OnActionBegin (button->second);
                    }
                }
                break;

            case SDL_JOYBUTTONUP:
#if defined (GP2X)
                switch (event.jbutton.button)
                {
                    case GP2X_BUTTON_CLICK:
                        clickPressed_ = false;
                        break;

                    default:
                        // Ignore.
                        break;
                }
#endif // GP2X
                {
                    JoyMap::iterator button (
                            joyMap_.find (event.jbutton.button));
                    if ( button != joyMap_.end () )
                    {
                        OnActionEnd (button->second);
                    }
                }
                break;

            case SDL_JOYAXISMOTION:
                if (SDL_X_AXIS == event.jaxis.axis)
                {
                    if (event.jaxis.value > DEAD_ZONE)
                    {
                        setXAxis(AxisRight);
                    }
                    else if (event.jaxis.value < -DEAD_ZONE)
                    {
                        setXAxis(AxisLeft);
                    }
                    else
                    {
                        setXAxis(AxisNone);
                    }
                }
                else if (SDL_Y_AXIS == event.jaxis.axis)
                {
                    if (event.jaxis.value > DEAD_ZONE)
                    {
                        setYAxis(AxisDown);
                    }
                    else if (event.jaxis.value < -DEAD_ZONE)
                    {
                        setYAxis(AxisUp);
                    }
                    else
                    {
                        setYAxis(AxisNone);
                    }
                }
                break;
        }
    }
}
