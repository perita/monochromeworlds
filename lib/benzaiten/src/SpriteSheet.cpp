//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // !HAVE_BENZAITEN_CONFIG_H
#include "SpriteSheet.hpp"
#include <cassert>

using namespace benzaiten;

SpriteSheet::SpriteSheet (const Surface &surface, int width, int height):
    sprites_ (),
    surface_ (surface)
{
    if ( height < 0 )
    {
        height = width;
    }

    assert ( width > 0 && "Invalid sprite sheet width" );
    assert ( height > 0 && "Index sprite sheet height" );

    int spritesX = surface.width () / width;
    int spritesY = surface.height () / height;

    for ( int y = 0 ; y < spritesY ; ++y )
    {
        for ( int x = 0 ; x < spritesX ; ++x )
        {
            SDL_Rect rect = { Sint16(x * width), Sint16(y * height), Uint16(width), Uint16(height) };
            sprites_.push_back (rect);
        }
    }
}

unsigned int
SpriteSheet::count () const
{
    return sprites_.size ();
}

size_t
SpriteSheet::height () const
{
    assert ( count () > 0 && "There is no sprites in this sheet" );
    // Just return the height of the first sprite.  All sprites have
    // the same height.
    return sprites_[0].h;
}

SDL_Rect
SpriteSheet::draw (int16_t x, int16_t y, unsigned int index,
        Surface &screen) const
{
    assert ( index < count () && "Invalid sheet index" );
    return surface_.blit (sprites_[index], x, y, screen);
}

void
SpriteSheet::setAlpha (uint8_t alpha)
{
    surface_.setAlpha (alpha);
}

size_t
SpriteSheet::width () const
{
    assert ( count () > 0 && "There is no sprites in this sheet" );
    // Just return the width of the first sprite.  All sprites have
    // the same height.
    return sprites_[0].w;
}
