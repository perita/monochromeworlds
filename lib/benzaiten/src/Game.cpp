//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // !HAVE_BENZAITEN_CONFIG_H
#include "Game.hpp"
#include <cassert>
#include <string>
#include <boost/bind.hpp>
#include "EventManager.hpp"
#include "FrameRateManager.hpp"
#include "GameStateManager.hpp"
#include "Surface.hpp"

#if defined(EMSCRIPTEN)
#include <emscripten.h>
#include <emscripten/html5.h>
#endif // EMSCRIPTEN

using namespace benzaiten;

Game::Game (EventManager &eventManager, GameStateManager &stateManager,
                Surface &screen, float fps):
    over_ (false)
    , eventManager_ (eventManager)
    , stateManager_ (stateManager)
    , screen_ (screen)
    , frameRateManager_ (fps)
{
}

void
Game::onQuit ()
{
    over (true);
}

inline bool
Game::over () const
{
    return over_;
}

inline void
Game::over (bool isOver)
{
    over_ = isOver;
}

void
Game::run ()
{
    // Connect the Quit signal so we know when the window closed.
    eventManager_.OnQuit.connect (boost::bind (&Game::onQuit, this));

#if defined(EMSCRIPTEN)
    emscripten_set_main_loop_arg(&Game::runOnce, this, 0, 1);
#else
    while ( !over () )
    {
        runOnce();
    }
#endif // !EMSCRIPTEN
}

void
Game::runOnce()
{
    if ( over () )
    {
        return;
    }

    frameRateManager_.update ();
    eventManager_.update ();

    if ( stateManager_.hasActiveState () )
    {
        IGameState &activeState (stateManager_.activeState (eventManager_));
        activeState.update (frameRateManager_.elapsedTime ());
        activeState.draw (screen_);

        screen_.flip ();
    }
    else
    {
        // If there's no active state, then we are done and
        // can quit the game.
        over (true);
    }
}

void
Game::runOnce(void *game) {
     static_cast<Game *>(game)->runOnce();
}
