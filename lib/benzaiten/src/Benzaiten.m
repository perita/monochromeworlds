/*
 * Benzaiten - A Simple Game Framework.
 * Copyright (C) 2008, 2009, 2010 Geisha Studios
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */
#import <Cocoa/Cocoa.h>
#import <SDL.h>
#import <stdlib.h> /* for setenv(). */
#import <unistd.h> /* for chdir(). */
#import <sys/param.h> /* for MAXPATHLEN */

@interface Benzaiten: NSObject
@end

#if defined(main)
#undef main
#endif /* main */

/* For some reason, Apple removed setAppleMenu from the headers in 10.4,
 * but the method is still there and works.  To avoid warnings, we declare
 * it ourselves here.
 */
@interface NSApplication(SDL_Missing_Methods)
- (void)setAppleMenu:(NSMenu *)menu;
@end

/* Portions of CPS.h */
typedef struct CPSProcessSerNum
{
    UInt32 lo;
    UInt32 hi;
} CPSProcessSerNum;

extern OSErr CPSEnableForegroundOperation(CPSProcessSerNum *psn,
        UInt32 arg2, UInt32 arg3, UInt32 arg4, UInt32 arg5);
extern OSErr CPSGetCurrentProcess(CPSProcessSerNum *psn);
extern OSErr CPSSetFrontProcess(CPSProcessSerNum *psn);
/* End of portions of CPS.h */

/* Global variables where the command line parameters' pointer get stored,
 * so we can pass them to SDL_main later.
 */
static int g_argc;
static char **g_argv;

/**
 * @brief Gets the application's name from the bundle.
 *
 * @return The string with the application's name.
 */
static NSString *
getApplicationName(void)
{
    NSDictionary *dict = 0;
    NSString *appName = 0;

    /* Determine the application's name from the bundle. */
    dict = (NSDictionary *)CFBundleGetInfoDictionary(CFBundleGetMainBundle());
    if (0 != dict)
    {
        appName = [dict objectForKey:@"CFBundleName"];
    }

    if (0 == [appName length])
    {
        appName = [[NSProcessInfo processInfo] processName];
    }

    return appName;
}

@interface BenzaitenApplication: NSApplication
@end

@implementation BenzaitenApplication
/**
 * @brief Posts an SDL_QUIT event.
 *
 * This is called from the `Quit' menu item.
 */
- (void)terminate: (id)sender
{
    SDL_Event quitEvent;

    quitEvent.type = SDL_QUIT;
    SDL_PushEvent(&quitEvent);
}

/**
 * @brief Ignores keystrokes which don't use the Command key.
 */
- (void)sendEvent: (NSEvent *)event
{
    if (NSKeyDown == [event type] || NSKeyUp == [event type])
    {
        if (NSCommandKeyMask == ([event modifierFlags] & NSCommandKeyMask))
        {
            [super sendEvent: event];
        }
        else
        {
            /* Ignore this key. */
        }
    }
    else
    {
        [super sendEvent: event];
    }
}
@end

@implementation Benzaiten
/**
 * @brief Sets the application's main menu and its items.
 */
static void
setApplicationMenu(void)
{
    NSMenu *appleMenu = 0;
    NSMenuItem *menuItem = 0;
    NSString *title = 0;
    NSString *appName = 0;

    appName = getApplicationName();
    appleMenu = [[NSMenu alloc] initWithTitle:@""];

    /* Add menu items. */
    title = [@"About " stringByAppendingString:appName];
    [appleMenu addItemWithTitle:title
                         action:@selector(orderFrontStandardAboutPanel:)
                  keyEquivalent:@""];

    [appleMenu addItem:[NSMenuItem separatorItem]];

    title = [@"Hide " stringByAppendingString:appName];
    [appleMenu addItemWithTitle:title
                         action:@selector(hide:)
                  keyEquivalent:@"h"];

    menuItem = (NSMenuItem *)
        [appleMenu addItemWithTitle:@"Hide Others"
                             action:@selector(hideOtherApplications:)
                      keyEquivalent:@"h"];

    [menuItem
        setKeyEquivalentModifierMask:(NSAlternateKeyMask | NSCommandKeyMask)];

    [appleMenu addItemWithTitle:@"Show All"
                         action:@selector(unhideAllApplications:)
                  keyEquivalent:@""];

    [appleMenu addItem:[NSMenuItem separatorItem]];

    title = [@"Quit " stringByAppendingString:appName];
    [appleMenu addItemWithTitle:title
                         action:@selector(terminate:)
                  keyEquivalent:@"q"];

    /* Put the menu into the menu bar. */
    menuItem = [[NSMenuItem alloc] initWithTitle:@""
                                           action:nil
                                    keyEquivalent:@""];
    [menuItem setSubmenu:appleMenu];
    [[NSApp mainMenu] addItem:menuItem];

    /* Tell the application object that this is now the application menu. */
    [NSApp setAppleMenu:appleMenu];

    /* Finally give up our reference to the objects. */
    [appleMenu release];
    [menuItem release];
}

/**
 * @brief Sets the working directory to the bundle's resource directory.
 */
- (void)setUpWorkingDirectory: (BOOL)shouldChDir
{
    if (shouldChDir)
    {
        char resourcesDirectory[MAXPATHLEN];
        CFURLRef resourcesUrl =
            CFBundleCopyResourcesDirectoryURL(CFBundleGetMainBundle());
        if (CFURLGetFileSystemRepresentation(resourcesUrl, true,
                    (UInt8 *)resourcesDirectory, MAXPATHLEN))
        {
            int result = chdir(resourcesDirectory);
            result = result; // XXX: To shut up the compiler when NDEBUG.
            assert (result == 0);
        }
        CFRelease(resourcesUrl);
    }
}

/**
 * @brief Sets the Window menu.
 */
static void
setWindowMenu(void)
{
    NSMenu *windowMenu = 0;
    NSMenuItem *windowMenuItem = 0;
    NSMenuItem *menuItem = 0;

    windowMenu = [[NSMenu alloc] initWithTitle:@"Window"];

    /* Just `Minimize'. */
    menuItem = [[NSMenuItem alloc] initWithTitle:@"Minimize"
                                          action:@selector(performMiniaturize:)
                                   keyEquivalent:@"m"];
    [windowMenu addItem:menuItem];
    [menuItem release];

    /* Put the menu into the menu bar. */
    windowMenuItem = [[NSMenuItem alloc] initWithTitle:@""
                                                action:nil
                                         keyEquivalent:@""];
    [windowMenuItem setSubmenu:windowMenu];
    [[NSApp mainMenu] addItem:windowMenuItem];

    /* Tell the application object that this is now the window menu. */
    [NSApp setWindowsMenu:windowMenu];

    /* Finally give up our references to the objects. */
    [windowMenu release];
    [windowMenuItem release];
}

/**
 * @brief Replacement for NSApplicationMain.
 *
 * @param[in] argc The number of parameters passed to the executable.
 * @param[in] argv The array of parameters.  The last parameters is @c NULL.
 */
static void
BenzaitenMain(int argc, char **argv)
{
    Benzaiten *game = 0;
    NSAutoreleasePool *pool = [[NSAutoreleasePool alloc] init];

    /* Ensure that the application object is initialized. */
    [BenzaitenApplication sharedApplication];

    /* Tell the Dock about us. */
    {
        CPSProcessSerNum PSN;
        if ( 0 == CPSGetCurrentProcess(&PSN) )
        {
            if (0 == CPSEnableForegroundOperation(&PSN,
                        0x03, 0x3c, 0x2c, 0x1103))
            {
                if (0 == CPSSetFrontProcess(&PSN))
                {
                    [BenzaitenApplication sharedApplication];
                }
            }
        }
    }

    /* Set up the menu bar. */
    [NSApp setMainMenu:[[NSMenu alloc] init]];
    setApplicationMenu();
    setWindowMenu();

    /* Create Benzaiten and make it the application's delegate. */
    game = [[Benzaiten alloc] init];
    [NSApp setDelegate:game];

    /* Start the main event loop. */
    [NSApp run];

    /* Clean up. */
    [game release];
    [pool release];
}

/**
 * @brief Called when the internal ecent loop has just started running.
 */
- (void)applicationDidFinishLaunching: (NSNotification *)note
{
    int status = 0;

    /* Tell SDL to send key events to Cocoa, so shortcuts will work. */
    setenv("SDL_ENABLEAPPEVENTS", "1", 1);
    /* Change to the working directory. */
    [self setUpWorkingDirectory:true];
    /* Start the main application's code. */
    status = SDL_main(g_argc, g_argv);

    /* We're done. */
    exit (status);
}

/**
 * @brief Main executable's entry point.
 *
 * @warning This should *not* be SDL_main.
 *
 * @param[in] argc The number of parameters passed to the executable.
 * @param[in] argv An array with the parameters passed to the executable.  The
 *            last element is @c NULL.
 *
 * @return @c 0 on success.
 *
 */
int
main(int argc, char **argv)
{
    /*
     * Mac OS X passes "-psn" if the application was launched by double
     * clicking.
     */
    if (2 <= argc && strncmp (argv[1], "-psn", 4) == 0)
    {
        /* In this case, just copy the application's exectuable name. */
        g_argc = 1;
        g_argv = (char **)SDL_malloc (sizeof (char *) * 2);
        g_argv[0] = argv[0];
        g_argv[1] = NULL;
    }
    else
    {
        /* Otherwise, copy the parameters to a global variable. */
        int parameter = 0;

        g_argc = argc;
        g_argv = (char **)SDL_malloc(sizeof (char *) * (argc + 1));
        for (parameter = 0 ; parameter <= argc ; ++parameter)
        {
            g_argv[parameter] = argv[parameter];
        }
    }

    BenzaitenMain(argc, argv);
    return 0;
}
@end
