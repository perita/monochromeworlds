//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_LOGO_STATE_HPP)
#define GEISHA_STUDIOS_BENZAITEN_LOGO_STATE_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include "IGameState.hpp"
#include "Surface.hpp"

namespace benzaiten
{
    ///
    /// @class LogoState
    /// @brief Shows the logo a brief amount of time.
    ///
    class LogoState: public IGameState
    {
        public:
            ///
            /// @brief Constructor.
            ///
            /// @param[in] logo The logo to show.
            /// @param[in] time The time that @p logo should be visible,
            ///            in ms.
            /// @param[in] background The background color.
            ///
            LogoState (const Surface &logo, float time = 1000.0f,
                    const Surface::Color &background =
                        Surface::Color (0, 0, 0, 255));

            void draw (Surface &screen);
            void update (float elapsedTime);

        private:
            /// The background color.
            Surface::Color background_;
            /// The logo to show.
            Surface logo_;
            /// The time that the logo should be visible.
            float time_;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_LOGO_STATE_HPP
