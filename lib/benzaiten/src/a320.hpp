//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_A320_HPP)
#define GEISHA_STUDIOS_BENZAITEN_A320_HPP

#define A320_BUTTON_UP SDLK_UP
#define A320_BUTTON_DOWN SDLK_DOWN
#define A320_BUTTON_RIGHT SDLK_RIGHT
#define A320_BUTTON_LEFT SDLK_LEFT
#define A320_BUTTON_R SDLK_BACKSPACE
#define A320_BUTTON_L SDLK_TAB
#define A320_BUTTON_A SDLK_LCTRL
#define A320_BUTTON_B SDLK_LALT
#define A320_BUTTON_X SDLK_SPACE
#define A320_BUTTON_Y SDLK_LSHIFT
#define A320_BUTTON_SELECT SDLK_ESCAPE
#define A320_BUTTON_START SDLK_RETURN
#define A320_BUTTON_END SDLK_UNKNOWN

#endif // !GEISHA_STUDIOS_BENZAITEN_A320_HPP
