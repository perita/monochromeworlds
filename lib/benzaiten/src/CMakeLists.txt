# SDL and SDL_mixer are required in order to build and run, but the test fails
# with Emscripten even thought it always have them.
if (NOT EMSCRIPTEN)
	find_package(SDL REQUIRED)
	find_package(SDL_mixer REQUIRED)
endif ()
include_directories(${SDL_INCLUDE_DIR})
include_directories(${SDLMIXER_INCLUDE_DIR})

# And Boost's headers.
find_package(Boost REQUIRED)
include_directories(${Boost_INCLUDE_DIR})

# The library's sources for all platforms.
set(benzaiten_HEADERS
    Animation.hpp
    Audio.hpp
    BitmapFont.hpp
    EventManager.hpp
    FadeState.hpp
    FrameRateManager.hpp
    Game.hpp
    GameStateManager.hpp
    IGameState.hpp
    LoadTGA.hpp
    LogoState.hpp
    Music.hpp
    MusicMixer.hpp
    MusicNull.hpp
    ResourceManager.hpp
    SettingsBase.hpp
    Sound.hpp
    SoundMixer.hpp
    SoundNull.hpp
    Sprite.hpp
    SpriteSheet.hpp
    StarField.hpp
    Surface.hpp
    System.hpp
    )
set(benzaiten_SOURCES
    Animation.cpp
    Audio.cpp
    BitmapFont.cpp
    EventManager.cpp
    FadeState.cpp
    FrameRateManager.cpp
    Game.cpp
    GameStateManager.cpp
    MusicMixer.cpp
    LoadTGA.cpp
    LogoState.cpp
    ResourceManager.cpp
    SettingsBase.cpp
    SoundMixer.cpp
    Sprite.cpp
    SpriteSheet.cpp
    StarField.cpp
    Surface.cpp
    System.cpp
    )

# This is for UNIX platforms to build a relocatable application (i.e., an
# application that can be moved.)
if(UNIX AND NOT APPLE AND NOT GP2X)
    option(ENABLE_BINARY_RELOCATION "Enable making the application relocatable" ON)
    if(ENABLE_BINARY_RELOCATION)
        add_definitions(-DENABLE_BINRELOC)
    endif(ENABLE_BINARY_RELOCATION)

    list(APPEND benzaiten_HEADERS binreloc.h SettingsImplUnix.hpp)
    list(APPEND benzaiten_SOURCES binreloc.c SettingsImplUnix.cpp)
endif(UNIX AND NOT APPLE AND NOT GP2X)

# Mac OS X sources.
if(APPLE)
    list(APPEND benzaiten_HEADERS SettingsImplOSX.hpp)
    list(APPEND benzaiten_SOURCES SettingsImplOSX.cpp Benzaiten.m)
endif(APPLE)

# Windows sources.
if(WIN32)
    list(APPEND benzaiten_HEADERS SettingsImplWin32.hpp)
    list(APPEND benzaiten_SOURCES SettingsImplWin32.cpp)
endif(WIN32)

# GP2X sources.
if(GP2X)
    list(APPEND benzaiten_HEADERS SettingsImplUnix.hpp gp2x.hpp)
    list(APPEND benzaiten_SOURCES SettingsImplUnix.cpp)
endif(GP2X)

# A320 sources.
if(A320)
    list(APPEND a320.hpp)
endif(A320)

# Create the config.h file, add this directory to the include directories in
# order to find it, and tell the compiler to use it.
configure_file(${CMAKE_CURRENT_SOURCE_DIR}/benzaiten_config.h.cmake
    ${CMAKE_CURRENT_BINARY_DIR}/benzaiten_config.h @ONLY)
include_directories(${CMAKE_CURRENT_BINARY_DIR})
add_definitions(-DHAVE_BENZAITEN_CONFIG_H)
list(APPEND benzaiten_HEADERS ${CMAKE_CURRENT_BINARY_DIR}/benzaiten_config.h)

# Tell that the header files are actually headers.
set_source_files_properties(${benzaiten_HEADERS} PROPERTIES HEADER_FILE_ONLY ON)

# Create an static library.
add_library(benzaiten STATIC ${benzaiten_HEADERS} ${benzaiten_SOURCES})

# Tell which libraries need to link against.
target_link_libraries(benzaiten signals ${SDLMIXER_LIBRARY} ${SDL_LIBRARY})

# XXX: For some odd reason, I have to link against the vorbis library
# under GP2X.
if(GP2X OR A320)
    target_link_libraries(benzaiten vorbisidec)
endif(GP2X OR A320)
