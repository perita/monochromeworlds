//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_GAME_STATE_MANAGER_HPP)
#define GEISHA_STUDIOS_BENZAITEN_GAME_STATE_MANAGER_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <list>
#include <stack>
#include "IGameState.hpp"

namespace benzaiten
{
    namespace Fade
    {
        enum Fade
        {
            None = 1L << 0, /// No fade between states.
            In = 1L << 1, /// Fade in between the current state and the next.
            Out = 1L << 2, /// Fade out between the current state and the next.
            Flash = 1L << 3 /// Flash between the current state and the next.
        };

        ///
        /// @brief Bitwise AND between two fade types.
        ///
        /// @param[in] lhs The left hand side operator.
        /// @param[in] rhs The right hand side operator.
        ///
        /// @return The result of the bitwise AND between @p lhs and @p rhs
        ///         (i.e., @p lhs & @p rhs).
        ///
        inline Fade
        operator& (Fade lhs, Fade rhs)
        {
            return Fade (static_cast<int> (lhs) & static_cast<int> (rhs));
        }

        ///
        /// @brief Bitwise OR between to fade types.
        ///
        /// @param[in] lhs The left hand side operator.
        /// @param[in] rhs The right hand side operator.
        ///
        /// @return The result of the bitwise OR between @p lhs and @p rhs
        ///         (i.e., @p lhs | @p rhs).
        ///
        inline Fade
        operator| (Fade lhs, Fade rhs)
        {
            return Fade (static_cast<int> (lhs) | static_cast<int> (rhs));
        }
    }

    ///
    /// @class GameStateManager
    /// @brief Manages the game states' life cycles.
    ///
    /// The game state manager stores all created states in a stack-like
    /// fashion.  The state at the stack's top is called the active state
    /// and is the only one that should receive the events from input devices,
    /// updates its logic, and draw to the screen.  All other states are in
    /// sleep mode.
    ///
    class GameStateManager
    {
        public:
            ///
            /// @brief Constructor.
            ///
            GameStateManager ();

            ///
            /// @brief Gets the active state.
            ///
            /// Make sure that the manager has an active state before
            /// calling this function by calling hasActiveState() first.
            ///
            /// @param[in] eventManager The event manager to use to connect
            ///            the active state's signals to.
            ///
            /// @return The reference to the active state.
            ///
            IGameState &activeState (EventManager &eventManager);

            ///
            /// @brief Tells if the manager has an active state.
            ///
            /// This function must be called to check that there is
            /// an active state to work on.  It should be called before
            /// activeState() to make sure it works properly.
            ///
            bool hasActiveState () const;

            ///
            /// @brief Sets a new state as the active state.
            ///
            /// This only pushes the state in the top of the stack and,
            /// optionally, adds the transition states (fade in and/or fade
            /// out) in the stack.  The real work to active the state will be
            /// done later when calling activeState().
            ///
            /// @param[in] state The state to set as the active state.
            /// @param[in] fade The fade to set between the currently active
            ///            state and the new active state.  It can use
            ///            any combination of Fade::Fade.
            ///
            void setActiveState (IGameState::ptr state,
                    Fade::Fade fade = Fade::In | Fade::Out);

            ///
            /// @brief Removes the active state manager.
            ///
            /// @param[in] fade Tells which fade state to use to remove the
            ///            active state.
            ///
            void removeActiveState (Fade::Fade fade = Fade::Out);

        private:
            /// The pointer to the previous active state.
            IGameState *previousActiveState_;
            /// The stack of all live states.  Only the top is the active state.
            std::stack<IGameState::ptr> states_;
            /// The list of states to delete once no one else is using them.
            std::list<IGameState::ptr> statesToDelete_;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_GAME_STATE_MANAGER_HPP
