//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_SYSTEM_HPP)
#define GEISHA_STUDIOS_BENZAITEN_SYSTEM_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <SDL.h>
#include <stdexcept>
#include <string>
#include <boost/noncopyable.hpp>
#include "Surface.hpp"

namespace benzaiten
{
    // Forward declarations.
    class ResourceManager;

    ///
    /// @class System
    /// @brief System initialization.
    ///
    class System: public boost::noncopyable
    {
        ///
        /// @class Error
        /// @brief Thrown when a System's unspecified error is generated.
        ///
        class Error: public std::runtime_error
        {
            public:
                ///
                /// @brief Constructor.
                ///
                /// @param[in] message The system error message.
                ///
                Error (const std::string &message):
                    std::runtime_error (message)
                {
                }
        };

        public:
            ///
            /// @brief Initializes the system and all subsystems.
            ///
            /// @throw Error If there is an error initializing the system or
            ///        any subsystem.
            ///
            System ();

            ///
            /// @brief Cleans up the system and all subsystems.
            ///
            ~System ();

            ///
            /// \brief Sets the window icon.
            ///
            /// @note This function must be called before setVideoMode().
            ///
            /// @param[in] icon The icon to set to the window.
            ///
            void setIcon(Surface &icon);

            ///
            /// \brief Sets the window icon from a resource.
            ///
            /// @note This function must be called before setVideoMode().
            ///
            /// @note For some platforms there is no point in settings
            ///       the icon and thus this function does nothing.  This
            ///       is the case for Mac OS X, GP2X and A320.
            ///
            /// @param[in] resources The resource manager to use to load
            ///            the icon from.
            /// @param[in] iconName The name of the icon to load and
            ///            set to the window.  If this icon can't be loaded,
            ///            this function just does nothing and doesn't report
            ///            any error.
            ///
            void setIcon(const ResourceManager &resources,
                    const std::string &iconName);

            ///
            /// @brief Sets the window title and icon name.
            ///
            /// @note On some systems it is necessary to first intialize the
            ///       video mode in order set the title.
            ///
            /// @note Even though technically this could be an static function,
            ///       it isn't because we need the system initialized before
            ///       we can set the window's title.
            ///
            /// @param[in] title A UTF-8 encoded string which will serve as
            ///            the window title (the text at the top of the window)
            ///            and as the iconified window title (the text which
            ///            is displayed in the menu bar or desktop when
            ///            the window is minimized.)
            ///
            void setTitle (const std::string &title);

            ///
            /// @brief Set up a video mode with the specified settings.
            ///
            /// Sets up a video mode with the specified @p width, @p height,
            /// and @p bitsperpixel.
            ///
            /// @param[in] width The width of the video mode to set.
            /// @param[in] height The height of the video mode to set.
            /// @param[in] bitsperpixel The bits per pixel of the video mode
            ///            to set.  If it is 0, it is treated as the
            ///            current display bits per pixel.
            /// @param[in] flags The flags to use to initialize the video mode.
            ///
            /// @note A @p bitsperpixel of 24 uses the packed represenation of
            ///       3 bytes per pixel.  For a more common 4 bytes per pixel,
            ///       use a @p bitsperpixel of 32.  Somewhat oddly, both 15
            ///       and 16 will request a 2 bytes per pixel mode, but
            ///       different pixel formats.
            ///
            /// @note Use benzaiten::Surface::Software if you plan on doing
            ///       per-pixel manipulations, or blit surfaces with alpha
            ///       channels, and require high framerarte.  When you use
            ///       hadrware surfaces (benzaiten::Surface::Hardware), SDL
            ///       copies the surfaces from video memory to system memory
            ///       when you lock them, and back when you unlock them.  This
            ///       can casue a major performance hit.  Be aware that you may
            ///       request a hardware surface but receive a software surface.
            ///
            /// @throw Error If the video mode could not be set.
            ///
            Surface setVideoMode (int width, int height, int bitsperpixel = 0,
                    Surface::Flags flags = Surface::Software);
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_SYSTEM_HPP
