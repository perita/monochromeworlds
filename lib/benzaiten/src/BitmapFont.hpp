//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_FONT_HPP)
#define GEISHA_STUDIOS_BENZAITEN_FONT_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <map>
#include <string>
#include "Surface.hpp"

#if defined (A320)
namespace std
{
    typedef basic_string<wchar_t> wstring;
}
#endif // A320

namespace benzaiten
{
    ///
    /// @class BitmapFont
    /// @brief A bitmap based font.
    ///
    class BitmapFont
    {
        public:
            ///
            /// @brief Constructor.
            ///
            /// @param[in] characters The list characters available in
            ///            @p surface.
            /// @param[in] surface The surface where the font's bitmaps are.
            ///
            BitmapFont (const std::wstring &characters, const Surface &surface);

            ///
            /// @brief Draws an string to a surface.
            ///
            /// @param[in] text The text to write.
            /// @param[in] x The x coordinate to write the text to.
            /// @param[in] y The y coordinate to write the text to.
            /// @param[in] screen The surface to write the text to.
            ///
            /// @return The rectangle that was used to draw the text on.
            ///
            SDL_Rect draw (const std::wstring &text, int16_t x, int16_t y,
                    Surface &screen) const;

            ///
            /// @brief Draws an string to a surface.
            ///
            /// @param[in] text The text to write.
            /// @param[in] x The x coordinate to write the text to.
            /// @param[in] y The y coordinate to write the text to.
            /// @param[in] screen The surface to write the text to.
            ///
            /// @return The rectangle that was used to draw the text on.
            ///
            SDL_Rect draw (const std::string &text, int16_t x, int16_t y,
                    Surface &screen) const;

            ///
            /// @brief Gets the font's height.
            ///
            /// @return The font's height.
            ///
            uint16_t height () const;

            ///
            /// @brief Gets the width that a text will have.
            ///
            /// @param[in] text The text to get its width.
            /// @return The width, in pixels, that @p text will have.
            ///
            uint16_t width (const std::wstring &text) const;

            ///
            /// @brief Gets the width that a text will have.
            ///
            /// @param[in] text The text to get its width.
            /// @return The width, in pixels, that @p text will have.
            ///
            uint16_t width (const std::string &text) const;

        private:
            /// The map of characters and rectangles.
            std::map<std::wstring::value_type, SDL_Rect> characters_;
            /// The font's surface.
            mutable Surface surface_;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_FONT_HPP
