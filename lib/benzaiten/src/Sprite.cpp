//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // !HAVE_BENZAITEN_CONFIG_H
#include "Sprite.hpp"
#include <cassert>

using namespace benzaiten;

Sprite::Sprite (const Animations &animations):
    animations_ (animations),
    currentAnimation_ (0)
{
    assert ( animations.size () > 0 &&
            "There is no animation for this sprite.");
}

Sprite::Sprite (const Animation &animation):
    animations_ (1, animation),
    currentAnimation_ (0)
{
}

Sprite::Sprite (const SpriteSheet &spriteSheet, unsigned int sheetIndex):
    animations_ (),
    currentAnimation_ (0)
{
    animations_.push_back (Animation (spriteSheet, sheetIndex));
}

Sprite::Sprite (const Surface &surface):
    animations_ (),
    currentAnimation_ (0)
{
    animations_.push_back (Animation (
                SpriteSheet (surface, surface.width(), surface.height()), 0));
}

size_t
Sprite::countAnimations () const
{
    return animations_.size ();
}

Animation &
Sprite::currentAnimation ()
{
    assert ( currentAnimationIndex () < countAnimations () );
    return animations_[currentAnimationIndex ()];
}

const Animation &
Sprite::currentAnimation () const
{
    return static_cast<const Animation &> (
            const_cast<Sprite &> (*this).currentAnimation ());
}

Sprite::Animations::size_type
Sprite::currentAnimationIndex () const
{
    return currentAnimation_;
}

SDL_Rect
Sprite::draw (int16_t x, int16_t y, Surface &screen) const
{
    return currentAnimation ().draw (x, y, screen);
}

void
Sprite::firstFrame ()
{
    currentAnimation ().firstFrame ();
}

bool
Sprite::hasAnimationEnded () const
{
    return currentAnimation ().hasEnded ();
}

size_t
Sprite::height () const
{
    return currentAnimation ().height ();
}

void
Sprite::lastFrame ()
{
    currentAnimation ().lastFrame ();
}

void
Sprite::nextFrame ()
{
    currentAnimation ().nextFrame ();
}

void
Sprite::setAlpha (uint8_t alpha)
{
    currentAnimation ().setAlpha (alpha);
}

void
Sprite::setAnimation (Animations::size_type animation)
{
    assert ( animation < countAnimations () );
    currentAnimation_ = animation;
    firstFrame ();
}

size_t
Sprite::width () const
{
    return currentAnimation ().width ();
}
