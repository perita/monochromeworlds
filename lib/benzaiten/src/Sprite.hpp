//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_SPRITE_HPP)
#define GEISHA_STUDIOS_BENZAITEN_SPRITE_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <vector>
#include "Animation.hpp"

namespace benzaiten
{
    // Forward declarations.
    class SpriteSheet;

    ///
    /// @class Sprite
    /// @brief A position-less animated sprite.
    ///
    class Sprite
    {
        public:
            /// The list of animations.
            typedef std::vector<Animation> Animations;

            ///
            /// @brief Initializes an sprite with some animations.
            ///
            /// @param[in] animations The Animations to initialize the
            ///            Sprite with.  It must contain at least one
            ///            animation.
            ///
            Sprite (const Animations &animations);

            ///
            /// @brief Initializes an sprite with a single animation.
            ///
            /// @param[in] animation The sprite's animation.
            ///
            Sprite (const Animation &animation);

            ///
            /// @brief Initializes an sprite with an static animation.
            ///
            /// @param[in] spriteSheet The sprite sheet to get the sprite's
            ///            image from.
            /// @param[in] sheetIndex The index of the sheet to of the sprite's
            ///            image.
            ///
            Sprite (const SpriteSheet &spriteSheet, unsigned int sheetIndex);

            ///
            /// @brief Initializes an sprite with an static animation.
            ///
            /// @param[in] surface The surface to get the sprite's image from.
            ///
            Sprite(const Surface &surface);

            ///
            /// @brief Gets the number of animations.
            ///
            /// @return The number of animations.
            ///
            size_t countAnimations () const;

            ///
            /// @brief Gets the current animation's index.
            ///
            /// @return The current animation's index.
            ///
            Animations::size_type currentAnimationIndex () const;

            ///
            /// @brief Draws the current sprite's animation.
            ///
            /// @param[in] x The X position to draw the sprite to.
            /// @param[in] y The Y position to draw the sprite to.
            /// @param[in] screen The surface where to draw the sprite to.
            ///
            /// @return The rectangle where the sprite has been drawn to.
            ///
            SDL_Rect draw (int16_t x, int16_t y, Surface &screen) const;

            ///
            /// @brief Sets the first frame of the current animation.
            ///
            void firstFrame ();

            ///
            /// @brief Tells if the animation has ended.
            ///
            /// @return @c true if the current animation has ended.  This only
            ///         happens for non-loop animations.
            ///
            bool hasAnimationEnded () const;

            ///
            /// @brief The sprite's current animation's height.
            ///
            /// @return The height of the sprite's current animation.
            ///
            size_t height () const;

            ///
            /// @brief Sets the last frame of the current animation.
            ///
            void lastFrame ();

            ///
            /// @brief Sets the next frame of the current animation.
            ///
            void nextFrame ();

            ///
            /// @brief Sets the sprite's alpha value.
            ///
            /// @param[in] alpha The sprite's alpha value to set.
            ///
            void setAlpha (uint8_t alpha);

            ///
            /// @brief Sets the index of the current animation.
            ///
            /// @param[in] animation The index of the animation to set.  It
            ///            must be less than countAnimations().
            ///
            void setAnimation (Animations::size_type animation);

            ///
            /// @brief The sprite's current animation's width.
            ///
            /// @return The width of the sprite's current animation.
            ///
            size_t width () const;

        private:
            ///
            /// @brief Gets the reference to the current animation.
            ///
            /// @return The reference to the current animation.
            ///
            Animation &currentAnimation ();

            ///
            /// @brief Gets the constant reference to the current animation.
            ///
            /// @return The constant reference to the current animation.
            ///
            const Animation &currentAnimation () const;

            /// The sprite's animations.
            Animations animations_;
            /// The sprite's current animation.
            Animations::size_type currentAnimation_;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_SPRITE_HPP
