//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // !HAVE_BENZAITEN_CONFIG_H
#include "GameStateManager.hpp"
#include <cassert>
#include "FadeState.hpp"

using namespace benzaiten;

GameStateManager::GameStateManager ():
    previousActiveState_ (0),
    states_ (),
    statesToDelete_ ()
{
}

IGameState &
GameStateManager::activeState (EventManager &eventManager)
{
    assert ( hasActiveState () );

    // Get the active state, which is the stack's top state.
    IGameState *activeState (states_.top ().get ());
    assert ( 0 != activeState );

    // If we changed the active state from the last time, tell the new
    // active state to activate.
    if ( activeState != previousActiveState_ )
    {
        if ( 0 != previousActiveState_ )
        {
            previousActiveState_->isActive (false);
            previousActiveState_->disconnectSignals ();
        }
        activeState->isActive (true);
        activeState->connectSignals (eventManager);
        activeState->onActivate ();
        previousActiveState_ = activeState;
    }

    // Remove all states to delete.  Since they are smart pointers, only
    // clearing the list will remove the states.
    statesToDelete_.clear ();

    // Return the reference to the active state, which is the stack's top
    // state.
    return *activeState;
}

bool
GameStateManager::hasActiveState () const
{
    return !states_.empty ();
}

void
GameStateManager::removeActiveState (Fade::Fade fade)
{
    if ( hasActiveState () )
    {
        statesToDelete_.push_back (states_.top ());
        states_.pop ();

        if ( Fade::Out == (fade & Fade::Out) )
        {
            setActiveState (IGameState::ptr (new FadeOutState ()), Fade::None);
        }
    }
}

void
GameStateManager::setActiveState (IGameState::ptr state, Fade::Fade fade)
{
    if ( state )
    {
        state->setStateManager (this);
        states_.push (state);
        if ( Fade::In == (fade & Fade::In) )
        {
            setActiveState (
                    IGameState::ptr (new FadeInState (state, 0, 0, 0)),
                        Fade::None);
        }
        if ( Fade::Flash == (fade & Fade::Flash) )
        {
            setActiveState (
                    IGameState::ptr (new FadeInState (state, 255, 255, 255)),
                        Fade::None);
        }
        if ( Fade::Out == (fade & Fade::Out) )
        {
            setActiveState (IGameState::ptr (new FadeOutState ()), Fade::None);
        }
    }
}
