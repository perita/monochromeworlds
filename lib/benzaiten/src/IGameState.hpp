//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if !defined (GEISHA_STUDIOS_BENZAITEN_I_GAME_STATE_HPP)
#define GEISHA_STUDIOS_BENZAITEN_I_GAME_STATE_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <algorithm>
#include <cassert>
#include <vector>
#include <boost/bind.hpp>
#include <boost/shared_ptr.hpp>
#include "EventManager.hpp"

#define DECLARE_BENZAITEN_EVENT_TABLE() public: virtual void connectSignals (benzaiten::EventManager &)
#define BENZAITEN_EVENT_TABLE(className) void className::connectSignals (benzaiten::EventManager &eventManager)
#define BENZAITEN_EVENT0(eventName, handlerName) addEventHandler (eventManager.eventName.connect (boost::bind(handlerName, this)))
#define BENZAITEN_EVENT1(eventName, handlerName) addEventHandler (eventManager.eventName.connect (boost::bind(handlerName, this, _1)))

namespace benzaiten
{
    // Forward declarations.
    class GameStateManager;
    class Surface;

    ///
    /// @class IGameState
    /// @brief Interface for game states.
    ///
    /// This virtual class is the parent of all game states: main menu,
    /// configuration screen, game view, etc.
    ///
    class IGameState
    {
        public:
            /// An smart pointer to a IGameState class.
            typedef boost::shared_ptr<IGameState> ptr;

            ///
            /// @brief Constructor.
            ///
            IGameState ():
                active_ (false),
                handlers_ (),
                stateManager_ (0)
            {
            }

            ///
            /// @brief Virtual destructor.
            ///
            virtual ~IGameState () { }

            ///
            /// @brief Connects the state's signal.
            ///
            /// @param[in] eventHandler The event handler to connect the
            ///            signals to.
            ///
            virtual void
            connectSignals (EventManager &eventHandler)
            {
            }

            ///
            /// @brief Draws the state to the screen.
            ///
            /// This function must be called once per frame in order to
            /// render the state to the screen.
            ///
            /// @param[in] screen The surface where to draw the state to.
            ///
            virtual void draw (Surface &screen) = 0;

            ///
            /// @brief Disconnect all state's signals.
            ///
            void disconnectSignals ()
            {
                std::for_each (handlers_.begin (), handlers_.end (),
                        std::mem_fun_ref(&EventManager::Handler::disconnect));
                handlers_.clear ();
            }

            ///
            /// @brief Tells whether the state is the active.
            ///
            /// @return @c true if the state is the active, @c false otherwise.
            ///
            bool
            isActive () const
            {
                return active_;
            }

            ///
            /// @brief Sets whether the state is the active.
            ///
            /// @param[in] active Set to @c true when the state is the active,
            ///            @c false when not.
            ///
            void
            isActive (bool active)
            {
                active_ = active;
            }

            ///
            /// @brief The states becomes active.
            ///
            /// This is called each time the state becomes active.  Here
            /// the state calls initialization code when they become active
            /// and hence all subsystems are active.
            ///
            virtual void onActivate () { }

            ///
            /// @brief Sets the state manager that controls this state.
            ///
            /// @param[in] stateManager The pointer to the state manger
            ///            controlling this state.
            ///
            void
            setStateManager (GameStateManager *stateManager)
            {
                stateManager_ = stateManager;
            }

            ///
            /// @brief Updates the state's logic.
            ///
            /// This function must be called once per frame to give the
            /// state the chance to update its own logic.  During this time,
            /// the state does not render to the screen and tries to take
            /// as little as possible, since while updating no events can be
            /// processed.
            ///
            /// @param[in] elapsedTime The time that elapsed from the last
            ///            frame to the frame we are updating now, in
            ///            milliseconds.
            ///
            virtual void update (float elapsedTime) = 0;

        protected:
            ///
            /// @brief Adds a new event handler to the state.
            ///
            /// @param[in] handler The event handler to add.
            ///
            void
            addEventHandler (const EventManager::Handler &handler)
            {
                handlers_.push_back (handler);
            }

            ///
            /// @brief Gets the reference to the state manager.
            ///
            /// @return The reference to the state manager controlling this
            ///         state
            ///
            GameStateManager &
            stateManager ()
            {
                assert ( 0 != stateManager_ &&
                        "This state has no state manager" );
                return *stateManager_;
            }

        private:
            /// Tells if the state is the active.
            bool active_;
            /// The state's handlers.
            std::vector<EventManager::Handler> handlers_;
            /// The state manager that controls this state.
            GameStateManager *stateManager_;
    };
}

#endif // !GEISHA_STUDIOS_BENZAITEN_I_GAME_STATE_HPP
