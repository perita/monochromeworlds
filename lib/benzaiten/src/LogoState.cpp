//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // !HAVE_BENZAITEN_CONFIG_H
#include "LogoState.hpp"
#include "GameStateManager.hpp"

using namespace benzaiten;

LogoState::LogoState (const Surface &logo, float time,
        const Surface::Color &background):
    background_ (background),
    logo_ (logo),
    time_ (time)
{
}

void
LogoState::draw (Surface &screen)
{
    screen.fill (background_);
    logo_.blit (screen.width () / 2 - logo_.width () / 2,
            screen.height () / 2 - logo_.height () / 2, screen);
}

void
LogoState::update (float elapsedTime)
{
    time_ -= elapsedTime;
    if ( time_ < 0.0f )
    {
        stateManager ().removeActiveState ();
    }
}
