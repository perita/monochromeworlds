//
// Benzaiten - A Simple Game Framework.
// Copyright (C) 2008, 2009, 2010 Geisha Studios.
//
// This library is free software;  you can redistribute it and/or modify
// it under the terms of the GNU Lesser General Public License as published
// by the Free Software Foundation; either version 2.1 of the License, or
// (at your option) any later version.
//
// This library is distributed in hope that it will be useful, but WITHOUT
// ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or
// FITNESS FOR A PARTICULAR PURPOSE.  See the GNU Lesser General Public
// License for more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this library; if not, write to the Free Software Foundation, Inc.,
// 51 Franklin Street, Fifth Floor, Boston, MA  02110-1301  USA
//
#if defined (HAVE_BENZAITEN_CONFIG_H)
#include <benzaiten_config.h>
#endif // !HAVE_BENZAITEN_CONFIG_H
#include "FrameRateManager.hpp"
#include <SDL.h>

using namespace benzaiten;

namespace
{
    /// The number of milliseconds the elapsed time can be.
    const uint32_t k_MinElapsedTime = 10;
    /// The number of milliseconds to wait to compute the mean elapsed time.
    const uint32_t k_TimeToComputeMean = 4096;

    ///
    /// @brief Clamps a value between a minimum and a a maximum.
    ///
    /// @param[in] value The value to clamp.
    /// @param[in] min The minimum value that @p value can be.
    /// @param[in] max The maximum value that @p value can be.
    ///
    /// @return @p max if @p value is greater than @p max,
    ///         @p min if @p value is less than @min, or
    ///         @p value otherwise.
    ///
    template <typename T> T
    clamp (const T &value, const T &min, const T &max)
    {
        if ( value < min )
        {
            return min;
        }

        if ( value > max )
        {
            return max;
        }

        return value;
    }

    ///
    /// @brief Gets the number of milliseconds since the program's start.
    ///
    /// @return The milliseconds since the program has started.
    ///
    uint32_t
    getCurrentTime ()
    {
        return SDL_GetTicks ();
    }
}

FrameRateManager::FrameRateManager (float frameRate):
    elapsedTime_ (0),
    frameRate_ (static_cast<uint32_t> (1000.0f / frameRate)),
    lastTicks_ (getCurrentTime ()),
    meanElapsedTime_ (frameRate_),
    numUpdates_ (0),
    totalElapsedTime_ (0)
{
}

uint32_t
FrameRateManager::elapsedTime () const
{
    return elapsedTime_;
}

void
FrameRateManager::update ()
{
    elapsedTime_ = getCurrentTime () - lastTicks_;

    /* XXX: This is not correct at all...

    // Accumulate the elapsed time period and the number of updates.
    totalElapsedTime_ += elapsedTime_;
    ++numUpdates_;
    // Clamp the elapsed time between a minimum and the mean of the
    // previous computed period, to prevent a peak elapsed time too high
    // like 600ms or so.
    elapsedTime_ = clamp (elapsedTime_, k_MinElapsedTime, meanElapsedTime_);
    // If the previod is over, compute its mean elapsed time.
    if ( totalElapsedTime_ > k_TimeToComputeMean )
    {
        meanElapsedTime_ = totalElapsedTime_ / numUpdates_ + k_MinElapsedTime;
        totalElapsedTime_ = 0;
        numUpdates_ = 0;
    }

    */

    // We must take into account the delay time in order to compute correct
    // countdownd and such.  Therefore we need to get the lastTicks before
    // the sleep.
    lastTicks_ = getCurrentTime ();
#if !defined(EMSCRIPTEN)
    if ( frameRate_ > elapsedTime_ )
    {
        SDL_Delay (frameRate_ - elapsedTime_);
    }
#endif // !EMSCRIPTEN
}
