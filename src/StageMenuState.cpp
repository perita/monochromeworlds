//
// Monochrome Worlds - The White and Black Puzzle Game.
// Copyright (C) 2008, 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "StageMenuState.hpp"
#include <GameStateManager.hpp>
#include <ResourceManager.hpp>

using namespace monochromeworlds;

BENZAITEN_EVENT_TABLE (StageMenuState)
{
    BENZAITEN_EVENT1(OnMenuAction, &StageMenuState::onMenuAction);
}

StageMenuState::StageMenuState (const benzaiten::ResourceManager &resources):
    background_ (benzaiten::Surface::screen ()),
    menuText_ (resources.graphic ("StageMenu.tga")),
    numEntries_ (4),
    selected_ (0),
    selectSound_ (resources.try_sound("select.ogg"))
{
    benzaiten::Surface black (background_.width (), background_.height (), 24);
    black.fill (0, 0, 0);
    black.setAlpha (128);
    black.blit (0, 0, background_);
}

void
StageMenuState::draw (benzaiten::Surface &screen)
{
    const size_t entryHeight = 29;
    const size_t menuHeight = menuText_.height () / 2;
    const size_t centerX = screen.width () / 2 - menuText_.width () / 2;

    background_.blit (0, 0, screen);
    size_t y = screen.height () / 2 - menuHeight / 2;
    for ( int entry = 0 ; entry < numEntries_ ; ++entry, y += entryHeight )
    {
        menuText_.blit (0,
                entry * entryHeight + (entry == selected_ ? menuHeight : 0),
                menuText_.width (), entryHeight,
                centerX, y, screen);
    }
}

void
StageMenuState::onMenuAction (benzaiten::MenuAction action)
{
    switch (action)
    {
        case benzaiten::Select:
            selectSound_->play ();
            switch (selected_)
            {
                case 1:
                    OnUndo ();
                    break;

                case 2:
                    OnReset ();
                    break;

                case 3:
                    OnQuit ();
                    break;

                default:
                    OnResume ();
                    break;
            }
            // falling over Cancel.
        case benzaiten::Cancel:
            stateManager ().removeActiveState (benzaiten::Fade::None);
            break;

        case benzaiten::Down:
            ++selected_;
            if ( selected_ >= numEntries_ )
            {
                selected_ = 0;
            }
            break;

        case benzaiten::Up:
            --selected_;
            if ( selected_ < 0 )
            {
                selected_ = numEntries_ - 1;
            }
            break;

        default:
            // Ignore.
            break;
    }
}

void
StageMenuState::update (float /*elapsedTime*/)
{
}

