//
// Monochrome Worlds - The White and Black Puzzle Game.
// Copyright (C) 2008, 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_MONOCHROME_WORLDS_STAGE_SELECT_STATE_HPP)
#define GEISHA_STUDIOS_MONOCHROME_WORLDS_STAGE_SELECT_STATE_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <string>
#include <utility>
#include <vector>
#include <BitmapFont.hpp>
#include <IGameState.hpp>
#include <Music.hpp>
#include <Sound.hpp>
#include <Surface.hpp>
#include <SpriteSheet.hpp>

namespace benzaiten
{
    // Forward declarations.
    class ResourceManager;
}

namespace monochromeworlds
{
    // Forward declarations.
    class Settings;

    ///
    /// @class StageSelectState
    /// @brief Allows the player to select which stage to play.
    ///
    class StageSelectState: public benzaiten::IGameState
    {
        public:
            enum
            {
                BlocksSize = 24,
                CheckSize = 32,
                Columns = 10,
                Rows = 2
            };

            ///
            /// @brief Constructor.
            ///
            /// @param[in] settings The settings to use.
            /// @param[in] resources The resource manager to use to
            ///            load graphics and stages.
            ///
            StageSelectState (Settings &settings,
                    const benzaiten::ResourceManager &resources);

            virtual void draw (benzaiten::Surface &screen);
            virtual void update (float elapsedTime);

        private:
            ///
            /// @brief Tells if an stage is clear.
            ///
            /// @param[in] stage The number of the stage (starting from 1)
            ///            to check whether is clear.
            /// @return @c true if @p stage is clear, @c false otherwise.
            ///
            bool isClear (std::size_t stage) const;

            ///
            /// @brief Tells if an stage is playable.
            ///
            /// @param[in] stage The number of the stage (starting from 1)
            ///            to check whether is playable.
            /// @return @c true if @p stage is playable, @c false otherwise.
            ///
            bool isPlayable (std::size_t stage) const;

            ///
            /// @brief Gets the resource manager.
            ///
            /// @return The constant reference to the resource manager.
            ///
            const benzaiten::ResourceManager &resources () const;

            ///
            /// @brief An action menu occurred.
            ///
            /// @param[in] action The action menu.
            ///
            void onMenuAction (benzaiten::MenuAction action);

            ///
            /// @brief The current stage have been cleared.
            ///
            void onStageCleared ();

            ///
            /// @brief Moves the current row one position down, if possible.
            ///
            void rowDown ();

            ///
            /// @brief Moves the current row one position up, if possible.
            ///
            void rowUp ();

            ///
            /// @brief Gets the selected stage.
            ///
            /// @return The number (starting from 1) of the selected stage.
            ///
            std::size_t selectedStage () const;

            ///
            /// @brief Gets the number of stages clear.
            ///
            /// @return The number of stages already clear.
            ///
            std::size_t stagesClear () const;

            /// The background image.
            benzaiten::Surface background_;
            /// The sprite sheet with the blocks to show as stages.
            benzaiten::SpriteSheet blocks_;
            /// The sprite sheet with the check and selection box.
            benzaiten::SpriteSheet check_;
            /// The currently selected column.
            int currentCol_;
            /// The currently selected row.
            int currentRow_;
            /// The font to use to print the stage's title and description.
            benzaiten::BitmapFont font_;
            /// The stage select label.
            benzaiten::Surface label_;
            /// The background music.
            benzaiten::Music::ptr music_;
            /// The resource manager.
            const benzaiten::ResourceManager &resources_;
            /// The select sound.
            benzaiten::Sound::ptr selectSound_;
            /// The settings.
            Settings &settings_;
            /// The number of stages clear.
            std::size_t stagesClear_;
            /// The description of each stage.
            std::vector<std::wstring> stagesDescription_;
            /// The tips of each stage.
            std::vector<std::pair<std::wstring, size_t> > stagesTip_;

            DECLARE_BENZAITEN_EVENT_TABLE ();
    };
}

#endif // !GEISHA_STUDIOS_MONOCHROME_WORLDS_STAGE_SELECT_STATE_HPP
