//
// Monochrome Worlds - The White and Black Puzzle Game.
// Copyright (C) 2008, 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_MONOCHROMEWORLDS_TITLE_STATE_HPP)
#define GEISHA_STUDIOS_MONOCHROMEWORLDS_TITLE_STATE_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <IGameState.hpp>
#include <BitmapFont.hpp>
#include <Sound.hpp>
#include <Surface.hpp>

namespace benzaiten
{
    // Forward declarations.
    class ResourceManager;
}

namespace monochromeworlds
{
    // Forward declarations.
    class Settings;

    ///
    /// @class TitleState
    /// @brief Shows the game's title.
    ///
    class TitleState: public benzaiten::IGameState
    {
        public:
            ///
            /// @brief Constructor.
            ///
            /// @param[in] settings The settings to use.
            /// @param[in] resources The resource manager to use.
            ///
            TitleState (Settings &settings,
                    const benzaiten::ResourceManager &resources);
            virtual void draw (benzaiten::Surface &screen);
            virtual void update (float elapsedTime);

        private:
            ///
            /// @brief An menu event fired.
            ///
            /// @param[in] action The action that fired.
            ///
            void onMenuAction (benzaiten::MenuAction action);

            ///
            /// @brief Gets the constant reference to the resources.
            ///
            /// @return The constant reference to the resource manager.
            ///
            const benzaiten::ResourceManager &resources () const;

            ///
            /// @brief Gets the references to the settings.
            ///
            /// @return The reference to the Settings.
            ///
            Settings &settings ();

            /// The title's background.
            benzaiten::Surface background_;
            /// The time to use to blink.
            float blink_;
            /// The credits text.
            benzaiten::Surface credits_;
            /// Tells if draw the text to the user.
            bool drawText_;
            /// The font to use to print the stage's title and description.
            benzaiten::BitmapFont font_;
            /// The resources.
            const benzaiten::ResourceManager &resources_;
            /// The select sound.
            benzaiten::Sound::ptr selectSound_;
            /// The settings.
            Settings &settings_;
            /// The title's lable.
            benzaiten::Surface title_;

            DECLARE_BENZAITEN_EVENT_TABLE ();
    };
}

#endif // !GEISHA_STUDIOS_MONOCHROMEWORLDS_TITLE_STATE_HPP
