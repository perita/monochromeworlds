//
// Monochrome Worlds - The White and Black Puzzle Game.
// Copyright (C) 2008, 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // !HAVE_CONFIG_H
#include "StageSelectState.hpp"
#include <sstream>
#include <GameStateManager.hpp>
#include <ResourceManager.hpp>
#include <boost/bind.hpp>
#include <boost/lexical_cast.hpp>
#include "Settings.hpp"
#include "Stage.hpp"
#include "StageState.hpp"
#include "StageView.hpp"

using namespace monochromeworlds;

BENZAITEN_EVENT_TABLE (StageSelectState)
{
    BENZAITEN_EVENT1(OnMenuAction, &StageSelectState::onMenuAction);
}

StageSelectState::StageSelectState (Settings &settings,
        const benzaiten::ResourceManager &resources):
    background_ (resources.graphic ("Background.tga")),
    blocks_ (resources.graphic ("Blocks.tga"), BlocksSize),
    check_ (resources.graphic ("Check.tga"), CheckSize),
    currentCol_ (0),
    currentRow_ (0),
    font_ (L" !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuvwxyz{|}~", resources.graphic ("Font.tga")),
    label_ (resources.graphic ("StageSelect.tga")),
    music_ (resources.try_music("bgm.ogg")),
    resources_ (resources),
    selectSound_ (resources.try_sound("select.ogg")),
    settings_ (settings),
    stagesClear_ (settings.getStagesClear ()),
    stagesDescription_ (),
    stagesTip_ ()
{
    stagesDescription_.push_back (L"Tutorial 1. This is what\nMonochrome Worlds is about.");
    stagesDescription_.push_back (L"Tutorial 2. Sometimes it is also\nnecessary to give up.");
    stagesDescription_.push_back (L"Tutorial 3. Try not to build a tall wall.");
    stagesDescription_.push_back (L"Tutorial 4. In some cases, changing\ncolors is not allowed.");
    stagesDescription_.push_back (L"Tutorial 5. You might be surprised.");
    stagesDescription_.push_back (L"Keep on climbing up\ntogether friendly.");
    stagesDescription_.push_back (L"You should attack from\nthe right side.");
    stagesDescription_.push_back (L"Black should make way for\nwhite to go through.");
    stagesDescription_.push_back (L"It looks scary but it is fairly simple.");
    stagesDescription_.push_back (L"You just should dig alternately\nto advance!");
    stagesDescription_.push_back (L"You can not go back from where\nyou have come.");
    stagesDescription_.push_back (L"The trick is to change places\nfrom left to right.");
    stagesDescription_.push_back (L"White must climp up again.");
    stagesDescription_.push_back (L"Offer! This time there are\nlots of goals.");
    stagesDescription_.push_back (L"A maze where streets are\nbeing changed? Think about passing\nthrough each other's way in order\nto advance.");
    stagesDescription_.push_back (L"Fall while making the stairway.\nYou will need to do a fairly\nsimple task.");
    stagesDescription_.push_back (L"In the central hole a path\nwhere black can climb must\nbe secured.");
    stagesDescription_.push_back (L"This is the stage where left and right\nare cleary divided.  Your luck\ndepends on how you go through\nthe middle.");
    stagesDescription_.push_back (L"I'll give you the most troublesome\nat the toppest scale.");
    stagesDescription_.push_back (L"The last one is the difficult!\nCan you really solve it?");

#if defined (GP2X) || defined (A320)
    stagesTip_.push_back (std::make_pair (L"Press [B] to put or remove blocks at feet. Press\n[A] to switch characters.  Black can walk on\nwhite and White on black. Help each other to\nreach the grey goal.", 4));
    stagesTip_.push_back (std::make_pair (L"If any of them gets stalled press [Start] to start\nover. Move thinking about what will be going on.", 2));
#else // !GP2X && !A320
    stagesTip_.push_back (std::make_pair (L"Press [Z] to put or remove blocks at feet. Press\n[Shift] to switch characters.  Black can walk on\nwhite and White on black. Help each other to\nreach the grey goal.", 4));
    stagesTip_.push_back (std::make_pair (L"If any of them gets stalled press [C] to start over.\nMove thinking about what will be going on.", 2));
#endif // GP2X || A320
    stagesTip_.push_back (std::make_pair (L"Can climb over a wall of one. So, it is not safe to\nmake walls of two. Press [X] repeatelly to rewind\nthe actions. Remember this convenient feature.", 3));
    stagesTip_.push_back (std::make_pair (L"It is not possible to change the color where the X\nmarks are. You will need you partner's help.", 2));
    stagesTip_.push_back (std::make_pair (L"Passing through the ! marks changes the color.\nIt is essential to change the colors in order.", 2));
    stagesTip_.push_back (std::make_pair (L"", 0));
    stagesTip_.push_back (std::make_pair (L"", 0));
    stagesTip_.push_back (std::make_pair (L"", 0));
    stagesTip_.push_back (std::make_pair (L"", 0));
    stagesTip_.push_back (std::make_pair (L"", 0));
    stagesTip_.push_back (std::make_pair (L"Congratulations, you have solved 10 stages.\nFrom now on you will find trickier problems.", 2));
    stagesTip_.push_back (std::make_pair (L"", 0));
    stagesTip_.push_back (std::make_pair (L"", 0));
    stagesTip_.push_back (std::make_pair (L"", 0));
    stagesTip_.push_back (std::make_pair (L"", 0));
    stagesTip_.push_back (std::make_pair (L"", 0));
    stagesTip_.push_back (std::make_pair (L"It is the final stretch now.", 1));
    stagesTip_.push_back (std::make_pair (L"", 0));
    stagesTip_.push_back (std::make_pair (L"", 0));
    stagesTip_.push_back (std::make_pair (L"If you have come this far then you will have\nno problems in solving this one.", 2));

    music_->play ();
}

void
StageSelectState::draw (benzaiten::Surface &screen)
{
    const int16_t startX = 4;
    const int16_t startY = 48;
    const int16_t spacing = 32;
    const int16_t checkOffset = (CheckSize - BlocksSize) / 2;

    background_.blit (0, 0, screen);
    label_.blit (startX, 5, screen);

    std::size_t stage = 1;
    for ( int row = 0 ; row < Rows ; ++row )
    {
        int16_t y = startY + row * spacing;
        for ( int col = 0 ; col < Columns ; ++col, ++stage )
        {
            int16_t x = startX + col * spacing;
            blocks_.draw (x, y, isPlayable (stage) ? 0 : 1, screen);
            if ( isClear (stage) )
            {
                check_.draw (x - checkOffset, y - checkOffset, 0, screen);
            }
            if ( row == currentRow_ && col == currentCol_ )
            {
                check_.draw (x - checkOffset, y - checkOffset, 1, screen);
            }
        }
    }

    std::string stageLabel ("Stage ");
    try
    {
        int16_t fontY = startY + Rows * spacing + spacing / 2;
        font_.draw (stageLabel +
                boost::lexical_cast<std::string> (selectedStage ()),
                startX, fontY, screen);

        fontY += font_.height ();
        std::wstring description (L"???");
        if ( isPlayable (selectedStage ()) )
        {
            description = stagesDescription_[selectedStage () - 1];
        }
        font_.draw (description, startX, fontY, screen);
    }
    catch (boost::bad_lexical_cast &)
    {
        // Swallow it.
    }
}

bool
StageSelectState::isClear (std::size_t stage) const
{
    return (stage - 1) < stagesClear ();
}

bool
StageSelectState::isPlayable (std::size_t stage) const
{
    return isClear (stage) || (stage - 1) == stagesClear ();
}

void
StageSelectState::onMenuAction (benzaiten::MenuAction action)
{
    switch (action)
    {
        case benzaiten::Cancel:
            stateManager ().removeActiveState ();
            break;

        case benzaiten::Select:
            {
                std::size_t currentStage = selectedStage ();
                if ( isPlayable (currentStage) )
                {
                    std::ostringstream fileName;
                    fileName << resources ().getFilePath ("stages", "stage");
                    fileName << currentStage << ".mws";

                    Stage stage (Stage::load (fileName.str ()));
                    std::pair<std::wstring, size_t> tip (
                            stagesTip_[currentStage - 1]);
                    StageView stageView (resources (), tip.first, tip.second);

                    boost::shared_ptr<StageState> stageState (
                            new StageState (resources (), stage, stageView,
                                StageLabel (resources (), currentStage)));
                    stageState->OnStageClear.connect (
                            boost::bind (&StageSelectState::onStageCleared,
                                this));
                    stateManager ().setActiveState (stageState,
                            benzaiten::Fade::Flash);
                    selectSound_->play ();
                }
            }
            break;

        case benzaiten::Up:
            rowUp ();
            break;

        case benzaiten::Down:
            rowDown ();
            break;

        case benzaiten::Left:
            if ( currentCol_ == 0 )
            {
                rowUp ();
                currentCol_ = Columns - 1;
            }
            else
            {
                --currentCol_;
            }
            break;

        case benzaiten::Right:
            if ( currentCol_ == Columns - 1 )
            {
                rowDown ();
                currentCol_ = 0;
            }
            else
            {
                ++currentCol_;
            }

        default:
            // Ignore.
            break;
    }
}

void
StageSelectState::onStageCleared ()
{
    if ( !isClear (selectedStage ()) )
    {
        ++stagesClear_;
        settings_.setStagesClear (stagesClear_);
    }
}

const benzaiten::ResourceManager &
StageSelectState::resources () const
{
    return resources_;
}

void
StageSelectState::rowDown ()
{
    currentRow_ = std::min (currentRow_ + 1, Rows - 1);
}

void
StageSelectState::rowUp ()
{
    currentRow_ = std::max (currentRow_ - 1, 0);
}

std::size_t
StageSelectState::selectedStage () const
{
    return currentRow_ * Columns + currentCol_ + 1;
}

std::size_t
StageSelectState::stagesClear () const
{
    return stagesClear_;
}

void
StageSelectState::update (float /*elapsedTime*/)
{
}
