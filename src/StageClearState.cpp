//
// Monochrome Worlds - The White and Black Puzzle Game.
// Copyright (C) 2008, 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "StageClearState.hpp"
#include <algorithm>
#include <GameStateManager.hpp>
#include <ResourceManager.hpp>

using namespace monochromeworlds;

StageClearState::StageClearState (const benzaiten::ResourceManager &resources,
        float time):
    alpha_ (0),
    background_ (benzaiten::Surface::screen ()),
    message_ (background_.width (), 75, 32),
    time_ (time)
{
    // First fill with black.
    message_.fill (0, 0, 0);

    // The blit the part of the background where the message will go,
    // except for two pixel on top and two pixel at bottom.
    background_.blit (0, background_.height () / 2 - message_.height () / 2 + 2,
            message_.width (), message_.height () - 4, 0, 2, message_);

    // Create a "black image" and blit it over the message's background with
    // an alpha of 128, to show like a shadow.
    benzaiten::Surface black (message_.width (), message_.height () - 4, 24);
    black.fill (0, 0, 0);
    black.setAlpha (128);
    black.blit (0, 2, message_);

    // Load the "Stage Clear!" message and blit to the message surface.
    benzaiten::Surface stageClear (resources.graphic ("StageClear.tga"));
    stageClear.blit (message_.width () / 2 - stageClear.width () / 2,
            message_.height () / 2 - stageClear.height () / 2, message_);

    // Initialize the message's alpha.
    message_.setAlpha (alpha_);
}

void
StageClearState::update (float elapsedTime)
{
    if ( alpha_ < 255 )
    {
        alpha_ = (std::min (alpha_ + 5, 255));
        message_.setAlpha (alpha_);
    }
    else
    {
        time_ -= elapsedTime;
        if ( time_ < 0.0f )
        {
            stateManager ().removeActiveState ();
        }
    }
}

void
StageClearState::draw (benzaiten::Surface &screen)
{
    background_.blit (0, 0, screen);
    message_.blit (0, screen.height () / 2 - message_.height () / 2, screen);
}
