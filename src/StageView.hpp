//
// Monochrome Worlds - The White and Black Puzzle Game.
// Copyright (C) 2008, 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_MONOCHROME_WORLDS_STAGE_VIEW_HPP)
#define GEISHA_STUDIOS_MONOCHROME_WORLDS_STAGE_VIEW_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <string>
#include <utility>
#include <Sprite.hpp>
#include "Stage.hpp"

#if defined (A320)
namespace std
{
    typedef basic_string<wchar_t> wstring;
}
#endif // A320

namespace benzaiten
{
    // Forward declarations.
    class ResourceManager;
}

namespace monochromeworlds
{
    ///
    /// @class StageView
    /// @brief Draws an stage to a surface.
    ///
    class StageView
    {
        public:
            /// The offset of an actor.
            typedef std::pair<int, int> Offset;

            ///
            /// @brief Constructor.
            ///
            /// Loads the required resources to be able to draw stages to
            /// surfaces in draw().
            ///
            /// @param[in] resources The resource manager to use to load the
            ///            required resources.
            /// @param[in] tip The tip to show.
            /// @param[in] height The height, in lines, of @p tip.  If it is
            ///            set to 0, no tip will be shown.
            ///
            StageView (const benzaiten::ResourceManager &resources,
                    const std::wstring &tip = std::wstring (),
                    size_t tipHeight = 0);

            ///
            /// @brief Draws the specified stage showing either Black or White.
            ///
            /// @param[in] stage The stage to draw.
            /// @param[in] worlds Tells which world is the current.
            /// @param[in] screen The surface where to draw the stage to.
            ///
            void draw (const Stage &stage, Stage::World world,
                    benzaiten::Surface &screen);

            ///
            /// @brief Tells that an actor is falling.
            ///
            /// @param[in] world The actor that is falling.
            /// @param[in] direction The direction that the actor is facing.
            ///
            void fallActor (Stage::World world, Stage::Direction direction);

            ///
            /// @brief Gets the offset of an actor.
            ///
            /// @param[in] world The world of the actor to get the offset from.
            ///
            /// @return The offset of the actor in @p world.
            ///
            Offset getActorOffset (Stage::World world) const;

            ///
            /// @brief Tells that an actor is moving.
            ///
            /// @param[in] world The world of the actor to move.
            /// @param[in] direction The direction in which the actor is moving.
            /// @param[in] climb Tells if the actor is climbing a block.
            ///
            void moveActor (Stage::World world, Stage::Direction direction,
                    bool climb);

            ///
            /// @brief A block has been toggled.
            ///
            /// This makes the block fade from the background to full solid.
            ///
            /// @param[in] position The position where is the toggled block.
            ///
            void onBlockToggled (const Stage::Position &position);

            ///
            /// @brief Resets the wall sprites to their still image.
            ///
            void resetWorld ();

            ///
            /// @brief Tells that an actor is stopped.
            ///
            /// @param[in] world The world of the actor to stop.
            /// @param[in] direction The direction in which the actor stopped.
            ///
            void stopActor (Stage::World world, Stage::Direction direction);

            ///
            /// @brief Tells the wall blocks to switch to world.
            ///
            /// @param[in] world The world to change to.
            ///
            void switchToWorld (Stage::World world);

            ///
            /// @brief Toggles the tip on or off.
            ///
            void toggleTip ();

            ///
            /// @brief Updates the offset of an actor.
            ///
            /// @param[in] world The world of the actor to update its offset.
            ///
            void updateActorOffset (Stage::World world);

        private:
            ///
            /// @enum ActorAnimation.
            /// @brief The animation index for an actor.
            ///
            enum ActorAnimation
            {
                FallLeft = 5,
                FallRight = 2,
                MoveLeft = 4,
                MoveRight = 1,
                StopLeft = 3,
                StopRight = 0
            };

            ///
            /// @brief WallBlackAnimation.
            /// @enum The animation index for the wall actor.
            ///
            enum WallBlockAnimation
            {
                BlackToWhite = 1,
                Still = 0,
                WhiteToBlack = 2
            };

            ///
            /// @brief Gets the size of a block.
            ///
            /// @return The size of a block.  A block's width is the same
            ///         as its height.
            ///
            unsigned int blockSize () const;

            ///
            /// @brief Draws an actor.
            ///
            /// @param[in] world The world of the actor to draw.
            /// @param[in] stage The stage where the actor is from.
            /// @param[in] scroll The scroll to use, in blocks.
            /// @param[in] scrollOffset The pixel scroll offset.
            /// @param[in] focused Tells if this is the actor focused.
            /// @param[in] screen The surface where to draw the actor to.
            ///
            void drawActor (Stage::World world, const Stage &stage,
                    const Offset &scroll, const Offset &scrollOffset,
                    bool focused, benzaiten::Surface &screen) const;

            ///
            /// @brief Sets the actor's animation, if not already set.
            ///
            /// @param[in] world The world of the actor to set the animation.
            /// @param[in] animation The animation to set to the actor.
            ///
            void setActorAnimation (Stage::World world,
                    ActorAnimation animation);

            ///
            /// @brief Sets the offset of an actor.
            ///
            /// @param[in] world The actor's world to set the offset.
            /// @param[in] offset The offset to set.
            ///
            void setActorOffset (Stage::World world, const Offset &offset);

            /// The border, in pixels, of the tip.
            static const uint16_t TipBorder;

            /// The actors' sprites.
            std::vector<benzaiten::Sprite> actors_;
            /// The actor's offset.
            std::vector<Offset> actorsOffset_;
            /// The block's sprites.
            std::vector<benzaiten::Sprite> blocks_;
            /// The size of each block.
            unsigned int blockSize_;
            /// The focus of the selected actor.
            benzaiten::Sprite focus_;
            /// The current scroll, in blocks.
            Offset scroll_;
            /// The stage's tip.
            boost::shared_ptr<benzaiten::Surface> tip_;
            /// Tells if the tip is visible.
            bool tipVisible_;
            /// The toggled block alpha.
            uint8_t toggledBlockAlpha_;
            /// The toggled block position.
            Stage::Position toggledBlockPosition_;
    };
}

#endif // !GEISHA_STUDIOS_MONOCHROME_WORLDS_STAGE_VIEW_HPP
