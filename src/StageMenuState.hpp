//
// Monochrome Worlds - The White and Black Puzzle Game.
// Copyright (C) 2008, 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_MONOCHROMEWORLDS_STAGE_MENU_STATE_HPP)
#define GEISHA_STUDIOS_MONOCHROMEWORLDS_STAGE_MENU_STATE_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <IGameState.hpp>
#include <Sound.hpp>
#include <Surface.hpp>
#include <boost/signal.hpp>

namespace benzaiten
{
    // Forward declarations.
    class ResourceManager;
}

namespace monochromeworlds
{
    ///
    /// @class StageMenuState
    /// @brief Shows the stage menu.
    ///
    class StageMenuState: public benzaiten::IGameState
    {
        public:
            mutable boost::signal<void ()> OnResume;
            mutable boost::signal<void ()> OnUndo;
            mutable boost::signal<void ()> OnReset;
            mutable boost::signal<void ()> OnQuit;

            ///
            /// @brief Constructor.
            ///
            /// @param[in] resources The resource manager to use.
            ///
            StageMenuState (const benzaiten::ResourceManager &resources);

            virtual void draw (benzaiten::Surface &screen);
            virtual void update (float elapsedTime);

        private:
            ///
            /// @brief A menu action event.
            ///
            /// @param[in] action The menu action event.
            ///
            void onMenuAction (benzaiten::MenuAction action);

            /// The background.
            benzaiten::Surface background_;
            /// The menu entries.
            benzaiten::Surface menuText_;
            /// The number of entries.
            int numEntries_;
            /// The selected option.
            int selected_;
            /// The select sound.
            benzaiten::Sound::ptr selectSound_;

            DECLARE_BENZAITEN_EVENT_TABLE();
    };
}

#endif // !GEISHA_STUDIOS_MONOCHROMEWORLDS_STAGE_MENU_STATE_HPP
