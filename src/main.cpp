//
// Monochrome Worlds - The White and Black Puzzle Game.
// Copyright (C) 2008, 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // !HAVE_CONFIG_H
#include <cstdlib>
#include <iostream>
#include <Audio.hpp>
#include <EventManager.hpp>
#include <Game.hpp>
#include <GameStateManager.hpp>
#include <LogoState.hpp>
#include <ResourceManager.hpp>
#include <SDL_main.h>
#include <System.hpp>
#include "GameAction.hpp"
#include "Settings.hpp"
#include "Stage.hpp"
#include "StageState.hpp"
#include "StageView.hpp"
#include "TitleState.hpp"

using namespace monochromeworlds;
using namespace benzaiten;

///
/// @brief Application's main entry point.
///
/// @param[in] argc The number of parameters passed to the application.
/// @param[in] argv An array with the parameters passed to the application.  The
///            last element is a @c NULL.
///
/// @return @c EXIT_SUCCESS if everything was OK, @c EXIT_FAILURE otherwise.
///
int
main (int argc, char *argv[])
{
    try
    {
        System system;
        ResourceManager resources ("monochromeworlds");
        system.setIcon (resources, "Icon.tga");

        Settings settings;
        settings.parseCommandLine (argc, argv);

        if ( !settings.shouldQuit () )
        {
            Surface screen (system.setVideoMode (320, 240));
            system.setTitle ("Monochrome Worlds");

            Audio audio (44100, Audio::S16SYS, Audio::Stereo, 1024);

            GameStateManager gameStateManager;
            const std::string stageFile (settings.stageFile ());
            if ( stageFile.empty () )
            {
                gameStateManager.setActiveState (
                        IGameState::ptr (new TitleState (settings, resources)));
            }
            else
            {
                Stage stage (Stage::load (stageFile));
                StageView view (resources);
                gameStateManager.setActiveState (
                        IGameState::ptr (
                            new StageState (resources, stage, view,
                                    StageLabel (resources, 0))),
                        Fade::In);
            }

            EventManager eventManager;
            eventManager.mapKey (SDLK_ESCAPE, Pause);
            eventManager.mapKey (SDLK_LEFT, MoveLeft);
            eventManager.mapKey (SDLK_RIGHT, MoveRight);
            eventManager.mapKey (SDLK_SPACE, ToggleTip);
            eventManager.mapKey (SDLK_z, ToggleBlock);
            eventManager.mapKey (SDLK_RSHIFT, SwitchWorld);
            eventManager.mapKey (SDLK_LSHIFT, SwitchWorld);
            eventManager.mapKey (SDLK_x, Undo);
            eventManager.mapKey (SDLK_c, Reset);

#if defined (A320)
            eventManager.mapKey(A320_BUTTON_START, Pause);
            eventManager.mapKey(A320_BUTTON_B, ToggleBlock);
            eventManager.mapKey(A320_BUTTON_X, Undo);
            eventManager.mapKey(A320_BUTTON_A, SwitchWorld);
            eventManager.mapKey(A320_BUTTON_R, ToggleTip);
#endif // A320

#if defined (GP2X)
            eventManager.mapJoy (GP2X_BUTTON_A, SwitchWorld);
            eventManager.mapJoy (GP2X_BUTTON_B, ToggleBlock);
            eventManager.mapJoy (GP2X_BUTTON_LEFT, MoveLeft);
            eventManager.mapJoy (GP2X_BUTTON_RIGHT, MoveRight);
            eventManager.mapJoy (GP2X_BUTTON_SELECT, ToggleTip);
            eventManager.mapJoy (GP2X_BUTTON_START, Pause);
            eventManager.mapJoy (GP2X_BUTTON_X, Undo);
#endif // !GP2X

            Game game (eventManager, gameStateManager, screen);
            game.run ();
            return EXIT_SUCCESS;
        }
    }
    catch (std::exception &e)
    {
        std::cerr << "Fatal error: " << e.what () << std::endl;
    }
    catch (...)
    {
        std::cerr << "Unknown error" << std::endl;
    }
    return EXIT_FAILURE;
}
