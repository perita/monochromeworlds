//
// Monochrome Worlds - The White and Black Puzzle Game.
// Copyright (C) 2008, 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_MONOCHROME_WORLDS_UNDO_HPP)
#define GEISHA_STUDIOS_MONOCHROME_WORLDS_UNDO_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <boost/shared_ptr.hpp>

namespace monochromeworlds
{
    // Forward references.
    class Stage;

    ///
    /// @class IUndo
    /// @brief Interface class for all undo objects.
    ///
    class IUndo
    {
        public:
            /// A pointer to a IUndo object.
            typedef boost::shared_ptr<IUndo> ptr;

            ///
            /// @brief Destructor.
            ///
            virtual ~IUndo () { }

            ///
            /// @brief Undoes one operator.
            ///
            virtual void operator() () = 0;
    };

    ///
    /// @class UndoPositionAndDirection
    /// @brief Undoes changing the actor's position and direction.
    ///
    class UndoPositionAndDirection: public IUndo
    {
        public:
            ///
            /// @brief Contructor.
            ///
            /// @param[in] stage The stage where to get the current position
            ///            and direction.
            /// @param[in] view The view to set the actor's direction when
            ///            stopping.
            ///
            UndoPositionAndDirection (Stage &stage);

            virtual void operator() ();

        private:
            int blackDir;
            int blackX;
            int blackY;
            Stage &stage;
            int whiteDir;
            int whiteX;
            int whiteY;
    };


    ///
    /// @class UndoToggle
    /// @brief Undoes toggling a block.
    ///
    class UndoToggle: public IUndo
    {
        public:
            ///
            /// @brief Contructor.
            ///
            /// @param[in] x The X coordinate of the block toggled.
            /// @param[in] y The Y coordinate of the block toggled.
            /// @param[in] stage The stage where to get the current position
            ///            and direction.
            /// @param[in] view The view to set the actor's direction when
            ///            stopping.
            ///
            UndoToggle (int x, int y, Stage &stage);

            virtual void operator() ();

        private:
            Stage &stage;
            UndoPositionAndDirection undoPositionAndDirection;
            int x;
            int y;
    };
}

#endif // !GEISHA_STUDIOS_MONOCHROME_WORLDS_UNDO_HPP
