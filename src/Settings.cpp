//
// Monochrome Worlds - The White and Black Puzzle Game.
// Copyright (C) 2008, 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // !HAVE_CONFIG_H
#include "Settings.hpp"
#include <algorithm>
#include <iomanip>
#include <iostream>

using namespace monochromeworlds;

Settings::Settings():
    SettingsBase("monochromeworlds"),
    stageFile_ (),
    shouldQuit_ (false)
{
}

std::size_t
Settings::getStagesClear () const
{
    return std::max (getInteger ("stages", "clear", 0), 0);
}

void
Settings::parseCommandLine (int argc, char *argv[])
{
    for ( int paramIndex = 1 ; paramIndex < argc ; ++paramIndex )
    {
        std::string parameter (argv[paramIndex]);

        // If it doesn't start with hypen ('-'), then we assume is the level
        // file and be done with it.
        if ( parameter[0] != '-' )
        {
            stageFile (parameter);
            return;
        }
        // Otherwise is a parameter.
        else
        {
            // Help
            if ( parameter == "-h" || parameter == "--help" )
            {
                showCommandLineUsage ();
                shouldQuit (true);
                return;
            }

            // Version
            else if ( parameter == "-V" || parameter == "--version" )
            {
                showVersion ();
                shouldQuit (true);
                return;
            }

            // Unknown parameter.
            else
            {
                throw CommandLineError (std::string ("Unknown parameter: ") +
                        parameter);
            }
        }
    }
}

void
Settings::setStagesClear (std::size_t stages)
{
    setInteger ("stages", "clear", stages);
}

bool
Settings::shouldQuit () const
{
    return shouldQuit_;
}

void
Settings::shouldQuit (bool quit)
{
    shouldQuit_ = quit;
}

void
Settings::showCommandLineUsage ()
{
    using std::cout;
    using std::endl;
    using std::left;
    using std::right;
    using std::setw;

    const std::size_t optionWidth = 25;

    cout << "Usage: monochromeworlds [OPTION] [STAGE]\n\n";
    cout << "The black and white puzzle game\n\n";

    cout << left << setw (optionWidth) << "  -h, --help";
    cout << right << "display this help message and exit\n";

    cout << left << setw (optionWidth) << "  -V, --version";
    cout << right << "print version information\n";

#if defined (PACKAGE_BUGREPORT)
    cout << "\nReport bugs to <" << PACKAGE_BUGREPORT << ">";
#endif // PACKAGE_BUGREPORT

    cout << endl;
}

void
Settings::showVersion ()
{
#if defined (PACKAGE_NAME) && defined (PACKAGE_VERSION)
    std::cout << PACKAGE_NAME << " " << PACKAGE_VERSION << std::endl;
#endif // PACKAGE_NAME && PACKAGE_VERSION
    std::cout << "Copyright (c) 2008, 2009 Geisha Studios.\n";
    std::cout << "This is free software.  You may redistribute copies of it under the terms of\n";
    std::cout << "the GNU General Public License <http://www.gnu.org/licenses/gpl.html>.\n";
    std::cout << "There is NO WARRANTY, to the extent permitted by law.";
    std::cout << std::endl;
}

const std::string &
Settings::stageFile () const
{
    return stageFile_;
}

void
Settings::stageFile (const std::string &fileName)
{
    stageFile_ = fileName;
}
