//
// Monochrome Worlds - The White and Black Puzzle Game.
// Copyright (C) 2008, 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_MONOCHROME_WORLDS_STAGE_STATE_HPP)
#define GEISHA_STUDIOS_MONOCHROME_WORLDS_STAGE_STATE_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <IGameState.hpp>
#include <Sound.hpp>
#include "GameAction.hpp"
#include "Stage.hpp"
#include "StageLabel.hpp"
#include "StageView.hpp"


namespace benzaiten
{
    // Forward declarations.
    class ResourceManager;
}

namespace monochromeworlds
{
    ///
    /// @class StageState
    /// @brief Controls a single in game stage.
    ///
    class StageState: public benzaiten::IGameState
    {
        public:
            /// The stage has been cleared.
            mutable boost::signal<void ()> OnStageClear;

            ///
            /// @brief Constructor.
            ///
            /// @param[in] resources The resource manager to use.
            /// @param[in] stage The stage to control.
            /// @param[in] view The view to use to show the stage to the user.
            /// @param[in] stageLabel The stage label to show to the user.
            ///
            StageState (const benzaiten::ResourceManager &resources,
                    const Stage &stage, const StageView &view,
                    const StageLabel &stageLabel);

            // IGameState
            virtual void draw (benzaiten::Surface &screen);
            virtual void update (float elapsedTime);

        private:
            enum Direction
            {
                Left = -1,
                None = 0,
                Right = 1
            };

            ///
            /// @brief Gets the direction where to move the actor to.
            ///
            /// @return The direction to move the actor to.
            ///
            Direction direction () const;

            ///
            /// @brief A game action has began.
            ///
            /// @param[in] action The action that began.
            ///
            void onActionBegan (int action);

            ///
            /// @brief A game action has ended.
            ///
            /// @param[in] action The action that ended.
            ///
            void onActionEnded (int action);

            ///
            /// @brief Resets the time to wait before update.
            ///
            void resetTimeToUpdate ();

            ///
            /// @brief Gets the constant reference to the resources manager.
            ///
            /// @return The constant reference to the resources manager.
            ///
            const benzaiten::ResourceManager &resources () const;

            ///
            /// @brief Sets the time before to update the state,
            ///
            /// @param[in] time The time to wait before to update.
            ///
            void setTimeToUpdate (unsigned int time);

            ///
            /// @brief Gets the reference to the state's stage.
            ///
            /// @return The reference to the state's stage.
            ///
            Stage &stage ();

            ///
            /// @brief Stops boths actors on the view.
            ///
            void stopActors ();

            ///
            /// @brief The time remaining before update the state.
            ///
            /// @return The time to wait before updating the state.
            ///
            unsigned int timeToUpdate () const;

            ///
            /// @brief Updates the current actor's position.
            ///
            /// @param[in] toDirection The direction to update the actor's
            ///            position to.
            ///
            void updateActorPosition (Stage::Direction toDirection);

            ///
            /// @brief Gets the reference to the state's view.
            ///
            /// @return The reference to the state's view.
            ///
            StageView &view ();

            ///
            /// @brief Gets the current world.
            ///
            /// @return The current world.
            ///
            Stage::World world () const;

            ///
            /// @brief Sets the current world.
            ///
            /// @param[in] world The world to set.
            ///
            void world (Stage::World world);

            /// The current direction to move the actor to.
            Direction direction_;
            /// The stage's label.
            StageLabel label_;
            /// The resource manager.
            const benzaiten::ResourceManager &resources_;
            /// The sound when changing worlds.
            benzaiten::Sound::ptr soundChangeWorld_;
            /// The sound when toggling a block.
            benzaiten::Sound::ptr soundToggleBlock_;
            /// The state's stage.
            Stage stage_;
            /// The time to wait until update again.
            unsigned int timeToUpdate_;
            /// The state's view to show the stage to the user.
            StageView view_;
            /// The current focused world.
            Stage::World world_;

            DECLARE_BENZAITEN_EVENT_TABLE();
    };
}

#endif // !GEISHA_STUDIOS_MONOCHROME_WORLDS_STAGE_STATE_HPP
