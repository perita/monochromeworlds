//
// Monochrome Worlds - The White and Black Puzzle Game.
// Copyright (C) 2008, 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // !HAVE_CONFIG_H
#include "Undo.hpp"
#include "Stage.hpp"

using namespace monochromeworlds;

UndoPositionAndDirection::UndoPositionAndDirection (Stage &stage):
    blackDir (Stage::Left),
    blackX (-1),
    blackY (-1),
    stage (stage),
    whiteDir (Stage::Left),
    whiteX (-1),
    whiteY (-1)
{
    {
        Stage::Position position (stage.getActorPosition (Stage::BlackWorld));
        blackX = position.x;
        blackY = position.y;
        blackDir = stage.getActorDirection (Stage::BlackWorld);
    }
    {
        Stage::Position position (stage.getActorPosition (Stage::WhiteWorld));
        whiteX = position.x;
        whiteY = position.y;
        whiteDir = stage.getActorDirection (Stage::WhiteWorld);
    }
}

void
UndoPositionAndDirection::operator() ()
{
    Stage::Direction blackDirection = static_cast<Stage::Direction> (blackDir);
    Stage::Direction whiteDirection = static_cast<Stage::Direction> (whiteDir);

    stage.setActorPosition (Stage::BlackWorld, blackX, blackY);
    stage.setActorDirection (Stage::BlackWorld, blackDirection);
    stage.setActorPosition (Stage::WhiteWorld, whiteX, whiteY);
    stage.setActorDirection (Stage::WhiteWorld, whiteDirection);
}

UndoToggle::UndoToggle (int x, int y, Stage &stage):
    stage (stage),
    undoPositionAndDirection (stage),
    x (x),
    y (y)
{
}

void
UndoToggle::operator() ()
{
    // Must toggle the block before undoing the position, otherwise
    // we would have problems with the autotoggle blocks.
    stage.toggle (x, y, true);
    undoPositionAndDirection ();
}
