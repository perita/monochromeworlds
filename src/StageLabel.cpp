//
// Monochrome Worlds - The White and Black Puzzle Game.
// Copyright (C) 2008, 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "StageLabel.hpp"
#include <ResourceManager.hpp>
#include <boost/lexical_cast.hpp>

using namespace monochromeworlds;

StageLabel::StageLabel (const benzaiten::ResourceManager &resources,
        size_t stageNumber, float time):
    alpha_ (SDL_ALPHA_TRANSPARENT),
    background_ (320, 75, 24),
    border_ (320, 2, 24),
    direction_ (1),
    done_ (false),
    number_ (boost::lexical_cast<std::string> (stageNumber)),
    text_ (resources.graphic ("Stage.tga")),
    time_ (time)
{
    background_.fill (0, 0, 0);
    background_.setAlpha (alpha_);
    border_.fill (0, 0, 0);
    border_.setAlpha (alpha_);
    text_.setAlpha (alpha_);
}

void
StageLabel::update (float elapsedTime)
{
    if ( !done_ )
    {
        if ( direction_ > 0 )
        {
            if ( alpha_ < 255 )
            {
                alpha_ = std::min (alpha_ + 10, 255);
            }
            else if ( time_ > 0 )
            {
                time_ -= elapsedTime;
            }
            else
            {
                direction_ = -1;
            }
        }
        else
        {
            if ( alpha_ > 0 )
            {
                alpha_ = std::max (alpha_ - 10, 0);
            }
            else
            {
                done_ = true;
            }
        }
        background_.setAlpha (alpha_ / 2 + 1);
        border_.setAlpha (alpha_);
        text_.setAlpha (alpha_);
    }
}

void
StageLabel::draw (benzaiten::Surface &screen)
{
    if ( !done_ )
    {
        const size_t height = text_.height ();
        const size_t labelWidth = 116;
        const size_t numberWidth = 23;

        size_t startY = screen.height () / 2 - background_.height () / 2 -
            border_.height ();
        border_.blit (0, startY, screen);
        startY += border_.height ();
        background_.blit (0, startY, screen);
        startY += background_.height ();
        border_.blit (0, startY, screen);

        size_t textWidth = labelWidth + numberWidth * number_.length ();
        size_t startX = screen.width () / 2 - textWidth / 2;
        size_t labelY = screen.height () / 2 - height / 2;
        text_.blit (0, 0, labelWidth, height, startX, labelY, screen);
        startX += labelWidth;
        for (size_t charIndex = 0 ; charIndex < number_.length () ; ++charIndex)
        {
            text_.blit (labelWidth + (number_[charIndex] - '0') * numberWidth,
                    0, numberWidth, height, startX, labelY, screen);
            startX += numberWidth;
        }
    }
}
