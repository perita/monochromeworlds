//
// Monochrome Worlds - The White and Black Puzzle Game.
// Copyright (C) 2008, 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_MONOCHROME_WORLDS_SETTINGS_HPP)
#define GEISHA_STUDIOS_MONOCHROME_WORLDS_SETTINGS_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <SettingsBase.hpp>

namespace monochromeworlds
{
    ///
    /// @bclass CommandLineError
    /// @brief Thrown to report invalid command line parameters.
    ///
    class CommandLineError: public std::runtime_error
    {
        public:
            ///
            /// @brief Constructor.
            ///
            /// @param[in] message The error message.
            ///
            explicit CommandLineError (const std::string &message):
                std::runtime_error (message)
            {
            }
    };

    ///
    /// @class Settings
    /// @brief Manages the settings specific to monochromeworlds.
    ///
    class Settings: public benzaiten::SettingsBase
    {
        public:
            ///
            /// @brief Constructor.
            ///
            Settings ();

            ///
            /// @brief Gets the number of stages clear.
            ///
            /// @return The number of stages that are already clear.
            ///
            std::size_t getStagesClear () const;

            ///
            /// @brief Parses the command line parameters.
            ///
            /// Parses the command line parameters and executes the correct
            /// action depending on the actual parameter.
            ///
            /// @param[in] argc The number of parameters in the command line.
            ///            The must be at least one parameters, which is
            ///            the application's executable itself.
            /// @param[in] argv The list of command line parameters.  It must
            ///            have @p argc + 1 parameters, where the last
            ///            parameter is @c NULL.
            ///
            /// @throw CommandLineError if @argv contains an unknown parameter.
            ///
            void parseCommandLine (int argc, char *argv[]);

            ///
            /// @brief Sets the number of stages clear.
            ///
            /// @param[in] stages The number of stages clear.
            ///
            void setStagesClear (std::size_t stages);

            ///
            /// @brief Tells if the application should quit.
            ///
            /// Depending on some parameters on the command line (notably
            /// --version and --help) the application is expected to quit.
            ///
            /// This function just tells the application that it should quit,
            /// but it can be safely (although confusingly) ignored.
            ///
            /// @return @c true if any command line parameter passed to
            ///         parseCommandLine() function makes the user expect
            ///         that the application will quit instead of carrying on.
            ///
            bool shouldQuit () const;

            ///
            /// @brief Gets the stage file to open.
            ///
            /// @return The stage file to open.  If not stage file must be open
            ///         returns an empty string.
            ///
            const std::string &stageFile () const;

        private:
            ///
            /// @brief Sets the stage file to load.
            ///
            /// @param[in] fileName The name of the stage file to load.
            ///
            void stageFile (const std::string &fileName);

            ///
            /// @brief Sets whether the application should quit.
            ///
            /// @param[in] quit Set it to @c true to tell to the application
            ///            that it should quit.
            ///
            void shouldQuit (bool quit);

            ///
            /// @brief Shows the game's command line usage.
            ///
            static void showCommandLineUsage ();

            ///
            /// @brief Shows the game's version information.
            ///
            void showVersion ();

            /// The stage file to load.  Defaults to empty.
            std::string stageFile_;
            /// Tells if the application should quit or not.
            bool shouldQuit_;
    };
}

#endif // !GEISHA_STUDIOS_MONOCHROME_WORLDS_SETTINGS_HPP
