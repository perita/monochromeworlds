//
// Monochrome Worlds - The White and Black Puzzle Game.
// Copyright (C) 2008, 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_MONOCHROME_WORLDS_STAGE_CLEAR_STATE_HPP)
#define GEISHA_STUDIOS_MONOCHROME_WORLDS_STAGE_CLEAR_STATE_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <IGameState.hpp>
#include <Surface.hpp>

namespace benzaiten
{
    // Forward declarations.
    class ResourceManager;
}

namespace monochromeworlds
{
    ///
    /// @class StageClearState
    /// @brief Shows to the user the "Stage Clear" some seconds.
    ///
    class StageClearState: public benzaiten::IGameState
    {
        public:
            ///
            /// @brief Constructor.
            ///
            /// @param[in] resources The resource manager to use.
            /// @param[in] time The time to wait while showing the stage
            ///            clear message, in milliseconds.
            ///
            StageClearState (const benzaiten::ResourceManager &resources,
                    float time = 1000);

            virtual void update (float elapsedTime);
            virtual void draw (benzaiten::Surface &screen);

        private:
            /// The message's alpha.
            uint8_t alpha_;
            /// The background image.
            benzaiten::Surface background_;
            /// The stage clear message.
            benzaiten::Surface message_;
            /// The time the state will be showing the message, in milliseconds.
            float time_;
    };
}

#endif // !GEISHA_STUDIOS_MONOCHROME_WORLDS_STAGE_CLEAR_STATE_HPP
