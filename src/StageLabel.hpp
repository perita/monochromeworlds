//
// Monochrome Worlds - The White and Black Puzzle Game.
// Copyright (C) 2008, 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_MONOCHROME_WORLDS_STAGE_LABEL_HPP)
#define GEISHA_STUDIOS_MONOCHROME_WORLDS_STAGE_LABEL_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <string>
#include <Surface.hpp>

namespace benzaiten
{
    // Forward declarations.
    class ResourceManager;
}

namespace monochromeworlds
{
    ///
    /// @class StageLabel
    /// @brief Draws the stage label with the stage's number on the screen.
    ///
    class StageLabel
    {
        public:
            ///
            /// @brief Constructor.
            ///
            /// @param[in] resources The resource manager to use.
            /// @param[in] stageNumber The stage number to draw.
            /// @param[in] time The time that the label should be visible,
            ///            in milliseconds.
            ///
            StageLabel (const benzaiten::ResourceManager &resources,
                    size_t stageNumber, float time = 500);

            ///
            /// @brief Updates the label.
            ///
            /// @param[in] elapsedTime The time that passed since the last
            ///            call to this function, in milliseconds.
            ///
            void update (float elapsedTime);

            ///
            /// @brief Draws the label.
            ///
            /// @param[in] screen The surface to draw the label to.
            ///
            void draw (benzaiten::Surface &screen);

        private:
            /// The current label's alpha.
            uint8_t alpha_;
            /// The label's background.
            benzaiten::Surface background_;
            /// The label's border.
            benzaiten::Surface border_;
            /// The fade direction.
            int direction_;
            /// Tells if we are done with the label.
            bool done_;
            /// The stage number to draw.
            std::string number_;
            /// The label's text.
            benzaiten::Surface text_;
            /// The time that the label should be visible.
            float time_;
    };
}

#endif // !GEISHA_STUDIOS_MONOCHROME_WORLDS_STAGE_LABEL_HPP
