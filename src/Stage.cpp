//
// Monochrome Worlds - The White and Black Puzzle Game.
// Copyright (C) 2008, 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // !HAVE_CONFIG_H
#include "Stage.hpp"
#include <cassert>
#include <fstream>
#include <sstream>

using namespace monochromeworlds;

Stage::Stage (int width, int height):
    OnToggle (),
    actors_ (),
    blocks_ (boost::extents[height][width]),
    undo_ (),
    wallBlock_ (Block::Wall, Block::Untogglable)
{
    assert ( width > 0 && "The stage's width can't be 0." );
    assert ( height > 0 && "The stage's height can't be 0." );

    for ( int y = 0 ; y < height ; ++y )
    {
        for ( int x = 0 ; x < width ; ++x )
        {
            blocks_[y][x] = wallBlock_;
        }
    }
}

Stage::Stage (const Stage &stage):
    OnToggle (),
    actors_ (),
    blocks_ (stage.blocks_),
    undo_ (stage.undo_),
    wallBlock_ (stage.wallBlock_)
{
    // MSVC don't allow arrays in the initialization list.
    actors_[0] = stage.actors_[0];
    actors_[1] = stage.actors_[1];
}

void
Stage::appendUndo (IUndo::ptr undo)
{
    undo_.push (undo);
}

Stage::Actor &
Stage::getActor (World world)
{
    assert ( (world == BlackWorld || world == WhiteWorld) &&
            "Invalid actor's world" );
    return actors_[world];
}

const Stage::Actor &
Stage::getActor (World world) const
{
    return static_cast<Actor &> (const_cast<Stage &> (*this).getActor (world));
}

Stage::Direction
Stage::getActorDirection (World world) const
{
    return getActor (world).direction;
}

const Stage::Position &
Stage::getActorPosition (World world) const
{
    return getActor (world).position;
}

Stage::Block &
Stage::getBlock (int x, int y)
{
    // If the position is invalid, just return a reference to a constant
    // wall.
    if ( x < 0 || y < 0 || x >= width () || y >= height() )
    {
        return wallBlock_;
    }
    return blocks_[y][x];
}

int
Stage::height () const
{
    return blocks_.shape()[0];
}

bool
Stage::isActorFalling (World world) const
{
    Position pos (getActorPosition (world));
    return !isSolidBlock (world, pos.x, pos.y + 1);
}

bool
Stage::isSolidBlock (World world, int x, int y) const
{
    const Block &block ((*this)(x, y));
    return !(Block::Goal == block.type ||
                (BlackWorld == world && Block::White == block.type) ||
                (WhiteWorld == world && Block::Black == block.type));
}

Stage
Stage::load (const std::string &fileName)
{
    // These are the different block's types and their representation
    // on the stage's files.
    enum
    {
        WALL = '#',
        TOGGLABLE_BLACK = '0',
        TOGGLABLE_WHITE = '1',
        GOAL = 'G',
        UNTOGGLABLE_BLACK = 'X',
        UNTOGGLABLE_WHITE = '%',
        AUTOTOGGLE_BLACK = '!',
        AUTOTOGGLE_WHITE = '$'
    };

    // The directions that an actor can be.
    enum
    {
        LEFT = 'L',
        RIGHT = 'R'
    };

    ///
    /// @struct check_stream
    /// @brief Checks that the stream is correct or throw StageDataInvalidError.
    ///
    struct check_stream
    {
        check_stream (std::istream &in)
        {
            if ( !in )
            {
                throw StageDataInvalidError ();
            }
        }
    };

    std::ifstream file (fileName.c_str ());
    if ( !file )
    {
        throw StageFileNotFoundError (fileName);
    }

    // Get the stage's width and height.
    int width;
    check_stream (file >> width);
    int height;
    check_stream (file >> height);

    // Create the stage width the specified width and height.  It will
    // initialize each block as a wall.
    Stage stage (width, height);

    // Just skip this line.
    {
        std::string data;
        check_stream (std::getline (file, data));
    }

    // Now read and fill up the stage's grid with the blocks id.  Just
    // ignore any block that is not black or white, since the stage is
    // already prefilled with walls.
    for ( int y = 0 ; y < height ; ++y )
    {
        std::string rowData;
        check_stream (std::getline (file, rowData));
        std::istringstream row (rowData);
        for ( int x = 0 ; x < width ; ++x )
        {
            char block;
            check_stream (row >> block);

            switch ( block )
            {
                case GOAL:
                    stage.setBlock (x, y, Stage::Block::Goal,
                            Stage::Block::Untogglable);
                    break;

                case TOGGLABLE_BLACK:
                    stage.setBlock (x, y, Stage::Block::Black,
                            Stage::Block::Togglable);
                    break;

                case TOGGLABLE_WHITE:
                    stage.setBlock (x, y, Stage::Block::White,
                            Stage::Block::Togglable);
                    break;

                case UNTOGGLABLE_BLACK:
                    stage.setBlock (x, y, Stage::Block::Black,
                            Stage::Block::Untogglable);
                    break;

                case UNTOGGLABLE_WHITE:
                    stage.setBlock (x, y, Stage::Block::White,
                            Stage::Block::Untogglable);
                    break;

                case AUTOTOGGLE_BLACK:
                    stage.setBlock (x, y, Stage::Block::Black,
                            Stage::Block::Autotoggle);
                    break;

                case AUTOTOGGLE_WHITE:
                    stage.setBlock (x, y, Stage::Block::White,
                            Stage::Block::Autotoggle);
                    break;

                // Any other case, just assume that is a wall.
                default:
                    stage.setBlock (x, y, Stage::Block::Wall,
                            Stage::Block::Untogglable);
            }
        }
    }

    // Now get the black and white's actors position and directions.
    {
        int x;
        check_stream (file >> x);
        int y;
        check_stream (file >> y);
        char direction;
        check_stream (file >> direction);

        stage.setActorStartPositionAndDirection (Stage::BlackWorld, x, y,
              LEFT == direction ? Stage::Left : Stage::Right);
    }
    {
        int x;
        check_stream (file >> x);
        int y;
        check_stream (file >> y);
        char direction;
        check_stream (file >> direction);

        stage.setActorStartPositionAndDirection (Stage::WhiteWorld, x, y,
              LEFT == direction ? Stage::Left : Stage::Right);
    }

    return stage;
}

Stage::Position
Stage::moveActor (World world, Stage::Direction direction)
{
    Position pos (getActorPosition (world));
    Position oldPos = pos;

    // Below
    if ( !isSolidBlock (world, pos.x, pos.y + 1) )
    {
        ++pos.y;
    }
    else if ( !isSolidBlock (world, pos.x + direction, pos.y) )
    {
        pos.x += direction;
        if ( !isSolidBlock (world, pos.x, pos.y + 1) )
        {
            appendUndo (IUndo::ptr (new UndoPositionAndDirection (*this)));
        }
    }
    else if ( !isSolidBlock (world, pos.x + direction, pos.y - 1) &&
           !isSolidBlock (world, pos.x, pos.y - 1) )
    {
        pos = Position (pos.x + direction, pos.y - 1);
    }

    // Triggering an auto-toggle block.
    if ( pos != oldPos )
    {
        Block &block (getBlock (oldPos.x, oldPos.y));
        bool autoToggled = false;
        if ( Block::Autotoggle == block.attribute )
        {
            autoToggled = true;
            toggleBlock (oldPos.x, oldPos.y, false, block);
        }
        // Check to see if we moved through an autotoggle block
        // when going upstairs.
        if ( pos.y == oldPos.y - 1 )
        {
            Block &blockTop (getBlock (oldPos.x, pos.y));
            if ( Block::Autotoggle == blockTop.attribute )
            {
                // If the previous block was autotoggled, then
                // we need to set this actor position to be on that
                // block first before toggle and set the final position
                // again.
                if ( autoToggled )
                {
                    setActorPosition (world, oldPos.x, pos.y);
                }
                toggleBlock (oldPos.x, pos.y, false, blockTop);
            }
        }
    }

    setActorPosition (world, pos.x, pos.y);
    return pos;
}

const Stage::Block &
Stage::operator() (int x, int y) const
{
    return const_cast<Stage &> (*this).getBlock (x, y);
}

void
Stage::reset ()
{
    while ( undo () )
    {
        // Nothing, just keep undoing the last movements.
    }
    // Reset the initial positions of the actors.
    setActorPosition (BlackWorld,
            getActor (BlackWorld).startPosition.x,
            getActor (BlackWorld).startPosition.y);
    setActorDirection (BlackWorld, getActor (BlackWorld).startDirection);

    setActorPosition (WhiteWorld,
            getActor (WhiteWorld).startPosition.x,
            getActor (WhiteWorld).startPosition.y);
    setActorDirection (WhiteWorld, getActor (WhiteWorld).startDirection);
}

void
Stage::setBlock (int x, int y, Block::Type type, Block::Attribute attribute)
{
    if ( x < 0 || x >= width () )
    {
        throw InvalidPositionError ("block X coordinate invalid");
    }
    if ( y < 0 || y >= height () )
    {
        throw InvalidPositionError ("block Y coordinate invalid");
    }

    // If the block is neither black or white, then is untogglable.
    if ( Block::Black != type && Block::White != type )
    {
        attribute = Block::Untogglable;
    }

    blocks_[y][x] = Block (type, attribute);
}

void
Stage::setActorDirection (World world, Direction direction)
{
    getActor (world).direction = direction;
}

void
Stage::setActorPosition (World world, int x, int y)
{
    if ( x < 0 || x >= width () )
    {
        throw InvalidPositionError ("actor X coorindate invalid");
    }
    if ( y < 0 || y >= height () )
    {
        throw InvalidPositionError ("actor Y coordinate invalid");
    }

    if ( isSolidBlock (world, x, y) )
    {
        throw InvalidPositionError ("the actor can't be on this block");
    }

    getActor (world).position = Position (x, y);
}

void
Stage::setActorStartPositionAndDirection (World world, int x, int y,
                    Direction direction)
{
    setActorPosition (world, x, y);
    setActorDirection (world, direction);
    getActor (world).startPosition = Position (x, y);
    getActor (world).startDirection = direction;
}

bool
Stage::toggle (int x, int y, bool undo)
{
    Block &block (getBlock (x, y));
    Position pos (x, y);
    if ( (Block::Togglable == block.attribute &&
                pos != getActorPosition (BlackWorld) &&
                pos != getActorPosition (WhiteWorld) ) || undo )
    {
        toggleBlock (x, y, undo, block);
        return true;
    }
    return false;
}

void
Stage::toggleBlock (int x, int y, bool undo, Block &block)
{
    assert ( Block::Black == block.type || Block::White == block.type );
    assert ( Block::Togglable == block.attribute ||
            Block::Autotoggle == block.attribute );

    block.type = (Block::Black == block.type ? Block::White : Block::Black);
    if ( !undo )
    {
        appendUndo (IUndo::ptr (new UndoToggle (x, y, *this)));
        OnToggle (Position (x, y));
    }
}

bool
Stage::undo ()
{
    if ( !undo_.empty () )
    {
        IUndo::ptr undo (undo_.top ());
        (*undo) ();
        undo_.pop ();
        return true;
    }
    return false;
}

int
Stage::width () const
{
    return blocks_.shape()[1];
}
