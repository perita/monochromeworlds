//
// Monochrome Worlds - The White and Black Puzzle Game.
// Copyright (C) 2008, 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // HAVE_CONFIG_H
#include "TitleState.hpp"
#include <GameStateManager.hpp>
#include <ResourceManager.hpp>
#include "StageSelectState.hpp"

using namespace monochromeworlds;

BENZAITEN_EVENT_TABLE (TitleState)
{
    BENZAITEN_EVENT1 (OnMenuAction, &TitleState::onMenuAction);
}

TitleState::TitleState (Settings &settings,
        const benzaiten::ResourceManager &resources):
    background_ (resources.graphic ("Background.tga")),
    blink_ (0.0f),
    credits_ (resources.graphic ("Credits.tga")),
    drawText_ (true),
    font_ (L" !\"#$%&'()*+,-./0123456789:;<=>?@ABCDEFGHIJKLMNOPQRSTUVWXYZ[\\]^_`abcdefghijklmnopqrstuwvxyz{|}~", resources.graphic ("Font.tga")),
    resources_ (resources),
    selectSound_ (resources.try_sound("select.ogg")),
    settings_ (settings),
    title_ (resources.graphic ("Title.tga"))
{
}

void
TitleState::draw (benzaiten::Surface &screen)
{
    background_.blit (0, 0, screen);
    title_.blit (screen.width () / 2 - title_.width () / 2, 10, screen);
    credits_.blit (5, screen.height () - credits_.height (), screen);

#if defined (GP2X)
    std::wstring message (L"Press A or B Button");
#elif defined (A320) // !GP2X
    std::wstring message (L"Press Start Button");
#else // !A320
    std::wstring message (L"Press Enter key to Start");
#endif // GP2X

    if ( drawText_ )
    {
        font_.draw (message, screen.width () / 2 - font_.width (message) / 2,
                screen.height () / 2 - font_.height () / 2, screen);
    }
}

void
TitleState::onMenuAction (benzaiten::MenuAction action)
{
    switch (action)
    {
        case benzaiten::Select:
            selectSound_->play ();
            stateManager ().removeActiveState (benzaiten::Fade::None);
            stateManager ().setActiveState (IGameState::ptr (
                        new StageSelectState (settings (), resources ())));
            break;

#if !defined(EMSCRIPTEN)
        case benzaiten::Cancel:
            stateManager ().removeActiveState ();
            break;
#endif // !EMSCRIPTEN

        default:
            // Ignore.
            break;
    }
}

const benzaiten::ResourceManager &
TitleState::resources () const
{
    return resources_;
}

Settings &
TitleState::settings ()
{
    return settings_;
}

void
TitleState::update (float elapsedTime)
{
    blink_ -= elapsedTime;
    if ( blink_ < 0.0f )
    {
        blink_ = 250;
        drawText_ = !drawText_;
    }
}
