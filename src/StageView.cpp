//
// Monochrome Worlds - The White and Black Puzzle Game.
// Copyright (C) 2008, 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // !HAVE_CONFIG_H
#include "StageView.hpp"
#include <algorithm>
#include <cassert>
#include <BitmapFont.hpp>
#include <ResourceManager.hpp>
#include <Surface.hpp>

using namespace monochromeworlds;

namespace
{
    ///
    /// @brief Gets the most adecuate background color for a world.
    ///
    /// @param[in] world The world to get its background color.
    ///
    /// @return The background color for @p world.
    ///
    benzaiten::Surface::Color getBackgroundForWorld (Stage::World world)
    {
        assert ( (Stage::BlackWorld == world || Stage::WhiteWorld == world) &&
                "The world must be either Black or White" );

        // This is color for a white world.
        benzaiten::Surface::Color color (0, 0, 0);
        if ( Stage::BlackWorld == world )
        {
            color.red = color.green = color.blue = 255;
        }
        return color;
    }

    ///
    /// @brief Gets the block's sprite index for a given block's type.
    ///
    /// @param[in] type The block type to get its block's sprite index.
    ///
    /// @return The sprite's index for @p type.
    ///
    unsigned int getBlockSpriteForBlock (Stage::Block::Type type)
    {
        // Currently, the type and the sprite index are the same.
        return type;
    }

    ///
    /// @brief Gets the block's sprite index for a given block's attribute.
    ///
    /// @param[in] attribute The block's attribute to get its sprite index.
    /// @param[in] type The block's type.
    ///
    /// @return The sprite's index for @p attribute and @p type.
    ///
    unsigned int getBlockSpriteForBlock (Stage::Block::Attribute attribute,
            Stage::Block::Type type)
    {
        if ( Stage::Block::Untogglable == attribute )
        {
            return 4 + (Stage::Block::Black == type ? 0 : 1);
        }
        // Autotoggle.
        else
        {
            return 6 + (Stage::Block::Black == type ? 0 : 1);
        }
    }

    ///
    /// @brief Gets the scroll offset of an actor's position.
    ///
    /// @param[in] position The position, in blocks, of the actor
    ///            in the screen.
    /// @param[in] offset The offset of the actor relative to the
    ///            @p position.
    /// @param[in] screenRange The range of blocks of the screen.
    /// @param[in] width The width of the stage, in blocks.
    /// @param[in] height The height of the stage, in blocks.
    /// @param[out] scroll The scroll to apply to center the actor
    ///             in @p position in the screen.
    /// @param[out] scrollOffset The offset in pixels of the scroll.
    ///
    void getScroll (const Stage::Position &position,
                    const StageView::Offset &actorOffset,
                    const StageView::Offset &screenRange,
                    int width, int height,
                    StageView::Offset &scroll, StageView::Offset &scrollOffset)
    {
        const StageView::Offset center (std::make_pair (
                    screenRange.first / 2, screenRange.second / 2));
        const StageView::Offset minScroll (std::make_pair (-1, -1));
        const StageView::Offset maxScroll (std::make_pair (
                    width + 1 - screenRange.first,
                    height + 1 - screenRange.second));

        // Scroll in X.
        if ( (width - minScroll.first) < screenRange.first )
        {
            scroll.first = minScroll.first;
            scrollOffset.first = 0;
        }
        else
        {
            scroll.first = std::min (std::max (position.x - center.first,
                        minScroll.first), maxScroll.first);

            if ( position.x - scroll.first == center.first &&
                    ((actorOffset.first < 0 && scroll.first != minScroll.first) || actorOffset.first > 0) &&
                    ((actorOffset.first > 0 && scroll.first != maxScroll.first) || actorOffset.first < 0) )
            {
                scrollOffset.first = actorOffset.first;
            }
            else
            {
                scrollOffset.first = 0;
            }
        }

        // Scroll in Y.
        if ( (height - minScroll.second) < screenRange.second )
        {
            scroll.second = minScroll.second;
            scrollOffset.second = 0;
        }
        else
        {
            scroll.second = std::min (std::max (position.y - center.second,
                        minScroll.second), maxScroll.second);

            if ( position.y - scroll.second == center.second &&
                    ((actorOffset.second < 0 && scroll.second != minScroll.second) || actorOffset.second > 0) &&
                    ((actorOffset.second > 0 && scroll.second != maxScroll.second) || actorOffset.second < 0) )
            {
                scrollOffset.second = actorOffset.second;
            }
            else
            {
                scrollOffset.second = 0;
            }
        }
    }

    ///
    /// @brief Moves an offset closer to another.
    ///
    /// @param[in] origin The origin offset.
    /// @param[in] target The target offset.
    ///
    /// @return An offset from @p origin a bit closer to @p target.  One unit
    ///         at max.
    ///
    StageView::Offset
    moveScrollCloser (const StageView::Offset &origin,
            const StageView::Offset &target)
    {
        StageView::Offset direction (std::make_pair<int, int> (0, 0));
        if ( origin.first > target.first )
        {
            direction.first = -1;
        }
        else if ( origin.first < target.first )
        {
            direction.first = 1;
        }
        if ( origin.second > target.second )
        {
            direction.second = -1;
        }
        else if ( origin.second < target.second )
        {
            direction.second = 1;
        }

        return std::make_pair<int, int> (origin.first + direction.first,
                origin.second + direction.second);
    }
}

const uint16_t StageView::TipBorder = 5;

StageView::StageView (const benzaiten::ResourceManager &resources,
        const std::wstring &tip, size_t tipHeight):
    actors_ (),
    actorsOffset_ (2, std::make_pair<int, int> (0, 0)),
    blocks_ (),
    blockSize_ (24),
    focus_ (benzaiten::Animation (
                benzaiten::SpriteSheet (resources.graphic ("Focus.tga"), 72),
                5, 6, 2, false, benzaiten::Animation::Backward)),
    scroll_ (std::make_pair<int, int> (-1, -1)),
    tip_ (),
    tipVisible_ (true),
    toggledBlockAlpha_ (SDL_ALPHA_OPAQUE),
    toggledBlockPosition_ ()
{
    // Blocks.
    benzaiten::Surface blocksSurface (resources.graphic ("Blocks.tga"));
    blockSize_ = blocksSurface.width ();

    benzaiten::SpriteSheet blocksSheet (blocksSurface, blockSize ());

    // The black block.
    blocks_.push_back (benzaiten::Sprite (blocksSheet, 0));

    // The white block.
    blocks_.push_back (benzaiten::Sprite (blocksSheet, 1));

    // The wall block.
    std::vector<benzaiten::Animation> wallBlock;
    wallBlock.push_back (benzaiten::Animation (blocksSheet, 2)); // Still.
    wallBlock.push_back (benzaiten::Animation (blocksSheet, 5, 7, 2, false)); // Black to White.
    wallBlock.push_back (benzaiten::Animation (blocksSheet, 8, 7, 2, false, benzaiten::Animation::Backward));  // White to Black.
    blocks_.push_back (benzaiten::Sprite (wallBlock));

    // The goal block.
    benzaiten::Surface goalSurface (resources.graphic ("Goal.tga"));
    benzaiten::SpriteSheet goalSheet (goalSurface, blockSize ());
    blocks_.push_back (benzaiten::Sprite (benzaiten::Animation (goalSheet, 0, 2)));

    // The black untogglable block.
    blocks_.push_back (benzaiten::Sprite (blocksSheet, 12));

    // The white untogglable block.
    blocks_.push_back (benzaiten::Sprite (blocksSheet, 13));

    // The black autotoggle block.
    blocks_.push_back (benzaiten::Sprite (blocksSheet, 14));

    // The white autotoggle block.
    blocks_.push_back (benzaiten::Sprite (blocksSheet, 15));

    // Actors.
    benzaiten::Surface actorsSurface (resources.graphic ("Guy.tga"));
    benzaiten::SpriteSheet actorsSheet (actorsSurface, blockSize ());

    // The black actor.
    std::vector<benzaiten::Animation> blackActor;
    blackActor.push_back (benzaiten::Animation (actorsSheet, 0, 1)); // Stop right.
    blackActor.push_back (benzaiten::Animation (actorsSheet, 1, 2, 5, true,
                benzaiten::Animation::Backward)); // Move right.
    blackActor.push_back (benzaiten::Animation (actorsSheet, 2, 1)); // Fall right.
    blackActor.push_back (benzaiten::Animation (actorsSheet, 3, 1)); // Stop left.
    blackActor.push_back (benzaiten::Animation (actorsSheet, 4, 2, 5, true,
                benzaiten::Animation::Backward)); // Move Left.
    blackActor.push_back (benzaiten::Animation (actorsSheet, 5, 1)); // Fall Left.
    actors_.push_back (benzaiten::Sprite (blackActor));

    // The white actor.
    std::vector<benzaiten::Animation> whiteActor;
    whiteActor.push_back (benzaiten::Animation (actorsSheet, 6, 1)); // Stop right.
    whiteActor.push_back (benzaiten::Animation (actorsSheet, 7, 2, 5, true,
                benzaiten::Animation::Backward)); // Move right.
    whiteActor.push_back (benzaiten::Animation (actorsSheet, 8, 1)); // Fall right.
    whiteActor.push_back (benzaiten::Animation (actorsSheet, 9, 1)); // Stop left.
    whiteActor.push_back (benzaiten::Animation (actorsSheet, 10, 2, 5, true,
                benzaiten::Animation::Backward)); // Move Left.
    whiteActor.push_back (benzaiten::Animation (actorsSheet, 11, 1)); // Fall Left.
    actors_.push_back (benzaiten::Sprite (whiteActor));

    // If there's tip, then create the tip's surface.  This is a non-critical
    // functionality, so if it fails, pretend it didn't happen.
    if ( tipHeight > 0 )
    {
        try
        {
            benzaiten::BitmapFont font (
                    L" !,.1234567890?ABCDEFGHIJKLMNOPQRSTUVWXYZ[]abcdefghijklmnopqrstuvwxyz",
                    resources.graphic ("TipFont.tga"));
            benzaiten::Surface screen (benzaiten::Surface::screen ());
            benzaiten::Surface tipSurface (screen.width () - TipBorder * 2,
                    font.height () * tipHeight + TipBorder * 2, 24);
            tipSurface.fill (192, 192, 192);
            tipSurface.setAlpha (200);
            font.draw (tip, TipBorder, TipBorder, tipSurface);
            tip_.reset (new benzaiten::Surface (tipSurface));
        }
        catch (...)
        {
            // Ignore.
        }
    }

    // Don't show the focus.
    focus_.lastFrame ();
}

unsigned int
StageView::blockSize () const
{
    return blockSize_;
}

void
StageView::draw (const Stage &stage, Stage::World world,
        benzaiten::Surface &screen)
{
    static const Offset screenRange = std::make_pair<int, int> (
            screen.width () / blockSize (), screen.height () / blockSize ());
    const Stage::Block::Type invisibleBlock =
        (world == Stage::BlackWorld ? Stage::Block::White : Stage::Block::Black);
    const benzaiten::Surface::Color background (getBackgroundForWorld (world));

    // Update the animations of all sprites.
    std::for_each (blocks_.begin (), blocks_.end (),
            std::mem_fun_ref (&benzaiten::Sprite::nextFrame));
    std::for_each (actors_.begin (), actors_.end (),
            std::mem_fun_ref (&benzaiten::Sprite::nextFrame));
    focus_.nextFrame ();

    // Get the scroll position.
    Offset scroll;
    Offset scrollOffset;
    getScroll (stage.getActorPosition (world), getActorOffset (world),
            screenRange, stage.width (), stage.height (), scroll, scrollOffset);

    scroll_ = moveScrollCloser (scroll_, scroll);

    Stage::Position blockPosition (0, scroll_.second - 1);
    for (int y = -1, posY = -static_cast<int>(blockSize()) - scrollOffset.second ;
            y < screenRange.second + 1 ;
            ++y, ++blockPosition.y, posY += blockSize() )
    {
        blockPosition.x = scroll_.first - 1;
		for ( int x = -1, posX = -static_cast<int>(blockSize()) - scrollOffset.first ;
                x < screenRange.first + 1 ;
                ++x, ++blockPosition.x, posX += blockSize() )
        {
            const Stage::Block &block (
                    stage (blockPosition.x, blockPosition.y));

            bool fade = toggledBlockAlpha_ < SDL_ALPHA_OPAQUE &&
                    blockPosition == toggledBlockPosition_;
            if ( invisibleBlock == block.type )
            {
                screen.fill (background, posX, posY,
                        blockSize (), blockSize ());
                if ( fade )
                {
                    unsigned int blockSprite = getBlockSpriteForBlock (
                                block.type == Stage::Block::Black ?
                                              Stage::Block::White :
                                              Stage::Block::Black);
                    assert ( blockSprite < blocks_.size () );
                    blocks_[blockSprite].setAlpha (SDL_ALPHA_OPAQUE - toggledBlockAlpha_);
                    blocks_[blockSprite].draw (posX, posY, screen);
                    blocks_[blockSprite].setAlpha (SDL_ALPHA_OPAQUE);
                }
            }
            else
            {
                unsigned int blockSprite = getBlockSpriteForBlock (block.type);
                assert ( blockSprite < blocks_.size () );

                if ( fade )
                {
                    screen.fill (background, posX, posY,
                            blockSize (), blockSize ());
                    blocks_[blockSprite].setAlpha (toggledBlockAlpha_);
                }

                blocks_[blockSprite].draw (posX, posY, screen);

                if ( fade )
                {
                    blocks_[blockSprite].setAlpha (SDL_ALPHA_OPAQUE);
                }
            }

            if ( fade )
            {
                toggledBlockAlpha_ = std::min (SDL_ALPHA_OPAQUE, toggledBlockAlpha_ + 10);
            }

            // If it's non-togglable white or black block, show the attribute
            // on top of the block.
            if ( block.attribute != Stage::Block::Togglable &&
                    (Stage::Block::Black == block.type  ||
                     Stage::Block::White == block.type) )
            {
                unsigned int attributeSprite =
                    getBlockSpriteForBlock (block.attribute, block.type);
                assert ( attributeSprite < blocks_.size () );
                blocks_[attributeSprite].draw (posX, posY, screen);
            }
        }
    }

    drawActor (Stage::BlackWorld, stage, scroll_, scrollOffset,
            Stage::BlackWorld == world, screen);
    drawActor (Stage::WhiteWorld, stage, scroll_, scrollOffset,
            Stage::WhiteWorld == world, screen);

    if ( tip_ && tipVisible_ )
    {
        tip_->blit (TipBorder,
                screen.height () - (tip_->height () + TipBorder), screen);
    }
}

void
StageView::drawActor (Stage::World world, const Stage &stage,
        const Offset &scroll, const Offset &scrollOffset, bool focused,
        benzaiten::Surface &screen) const
{
    assert ( world == Stage::WhiteWorld || world == Stage::BlackWorld );
    assert ( static_cast<size_t> (world) < actors_.size () );

    const Stage::Position &position (stage.getActorPosition (world));
    Offset offset (getActorOffset (world));
    Offset actualPosition (std::make_pair (
                (position.x - scroll.first) * blockSize () +
                    offset.first - scrollOffset.first,
                (position.y - scroll.second) * blockSize () +
                    offset.second - scrollOffset.second));

    if ( focused && !focus_.hasAnimationEnded () )
    {
        focus_.draw (actualPosition.first - blockSize (),
                actualPosition.second - blockSize (), screen);
    }
    actors_[world].draw (actualPosition.first, actualPosition.second, screen);
}

void
StageView::fallActor (Stage::World world, Stage::Direction direction)
{
    setActorAnimation (world, Stage::Left == direction ? FallLeft : FallRight);
    setActorOffset(world, std::make_pair<int, int> (0, -1 * blockSize ()));
}

StageView::Offset
StageView::getActorOffset (Stage::World world) const
{
    assert ( world == Stage::WhiteWorld || world == Stage::BlackWorld );
    assert ( static_cast<size_t> (world) < actorsOffset_.size () );
    return actorsOffset_[world];
}
void
StageView::moveActor (Stage::World world, Stage::Direction direction,
        bool climb)
{
    setActorAnimation (world, Stage::Left == direction ? MoveLeft : MoveRight);
    Offset offset = std::make_pair<int, int> (
            direction == Stage::Left ? blockSize () : -1 * blockSize (),
            climb ? blockSize () : 0);
    setActorOffset (world, offset);
}

void
StageView::onBlockToggled (const Stage::Position &position)
{
    toggledBlockAlpha_ = 0;
    toggledBlockPosition_ = position;
}

void
StageView::resetWorld ()
{
    unsigned int blockSprite = getBlockSpriteForBlock (Stage::Block::Wall);
    assert ( blockSprite < blocks_.size () );
    blocks_[blockSprite].setAnimation (Still);
    focus_.lastFrame ();
}

void
StageView::setActorAnimation (Stage::World world, ActorAnimation animation)
{
    assert ( world == Stage::WhiteWorld || world == Stage::BlackWorld );
    assert ( static_cast<size_t> (world) < actors_.size () );
    if ( actors_[world].currentAnimationIndex () !=
            benzaiten::Sprite::Animations::size_type (animation) )
    {
        actors_[world].setAnimation (animation);
    }
}

void
StageView::setActorOffset (Stage::World world, const Offset &offset)
{
    assert ( world == Stage::WhiteWorld || world == Stage::BlackWorld );
    assert ( static_cast<size_t> (world) < actorsOffset_.size () );
    actorsOffset_[world] = offset;
}

void
StageView::stopActor (Stage::World world, Stage::Direction direction)
{
    setActorOffset (world, std::make_pair (0, 0));
    setActorAnimation (world, Stage::Left == direction ? StopLeft : StopRight);
}

void
StageView::switchToWorld (Stage::World world)
{
    assert ( world == Stage::WhiteWorld || world == Stage::BlackWorld );
    unsigned int blockSprite = getBlockSpriteForBlock (Stage::Block::Wall);
    assert ( blockSprite < blocks_.size () );
    blocks_[blockSprite].setAnimation (
            world == Stage::BlackWorld ? WhiteToBlack : BlackToWhite);
    toggledBlockAlpha_ = SDL_ALPHA_OPAQUE;
    focus_.firstFrame ();
}

void
StageView::toggleTip ()
{
    tipVisible_ = !tipVisible_;
}

void
StageView::updateActorOffset (Stage::World world)
{
    const int speed = 3;

    Offset offset = getActorOffset (world);
    if ( offset.first > 0 )
    {
        offset.first = std::max (offset.first - speed, 0);
    }
    else if ( offset.first < 0 )
    {
        offset.first = std::min (offset.first + speed, 0);
    }

    if ( offset.second > 0 )
    {
        offset.second = std::max (offset.second - speed * 2, 0);
    }
    else if ( offset.second < 0 )
    {
        offset.second = std::min (offset.second + speed, 0);
    }

    setActorOffset (world, offset);
}
