//
// Monochrome Worlds - The White and Black Puzzle Game.
// Copyright (C) 2008, 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if defined (HAVE_CONFIG_H)
#include <config.h>
#endif // !HAVE_CONFIG_H
#include "StageState.hpp"
#include <GameStateManager.hpp>
#include <ResourceManager.hpp>
#include <boost/bind.hpp>
#include "StageClearState.hpp"
#include "StageMenuState.hpp"

using namespace monochromeworlds;

BENZAITEN_EVENT_TABLE (StageState)
{
    BENZAITEN_EVENT1 (OnActionBegin, &StageState::onActionBegan);
    BENZAITEN_EVENT1 (OnActionEnd, &StageState::onActionEnded);
}

StageState::StageState (const benzaiten::ResourceManager &resources,
        const Stage &stage, const StageView &view,
        const StageLabel &stageLabel):
    direction_ (None),
    label_ (stageLabel),
    resources_ (resources),
    soundChangeWorld_ (resources.try_sound("world.ogg")),
    soundToggleBlock_ (resources.try_sound("block.ogg")),
    stage_ (stage),
    timeToUpdate_ (0),
    view_ (view),
    world_ (Stage::BlackWorld)
{
    stopActors ();
    stage_.OnToggle.connect (
            boost::bind (&StageView::onBlockToggled, &view_, _1));
    stage_.OnToggle.connect (
            boost::bind (&benzaiten::Sound::play, soundToggleBlock_.get ()));
}

StageState::Direction
StageState::direction () const
{
    return direction_;
}

void
StageState::draw (benzaiten::Surface &screen)
{
    view ().draw (stage (), world (), screen);
    label_.draw (screen);
}

void
StageState::onActionBegan (int action)
{
    switch (action)
    {
        case MoveLeft:
            direction_ = Left;
            break;

        case MoveRight:
            direction_ = Right;
            break;

        case SwitchWorld:
            // Only switch if the actor is not falling.
            if ( !stage ().isActorFalling (world ()) )
            {
                // First stop the actor in her own block.
                view ().stopActor (world (),
                        stage ().getActorDirection (world ()));

                // Now change the world and tell the view.
                world (world () == Stage::BlackWorld ?
                        Stage::WhiteWorld :
                        Stage::BlackWorld );
                view ().switchToWorld (world ());
                soundChangeWorld_->play ();
            }
            break;

        case ToggleBlock:
            {
                StageView::Offset offset (view ().getActorOffset (world ()));
                if ( 0 == offset.first && 0 == offset.second  &&
                        !stage ().isActorFalling (world ()) )
                {
                    Stage::Position pos (stage ().getActorPosition (world ()));
                    // The tile in front and below of us.
                    pos.x +=
                        (Stage::Left == stage ().getActorDirection (world ()) ?
                         -1 : 1);
                    ++pos.y;
                    stage ().toggle (pos.x, pos.y);
                }
            }
            break;

        case ToggleTip:
            view ().toggleTip ();
            break;

        case Undo:
            if ( stage ().undo () )
            {
                // Autotoggle blocks can put the actor to falling
                // position.  Just undo until we are on safe ground.
                while ( stage ().isActorFalling (world ()) &&
                        stage ().undo () )
                {
                    // Keep undoing!
                }
                stopActors ();
            }
            break;

        case Reset:
            stage ().reset ();
            world (Stage::BlackWorld);
            stopActors ();
            view ().resetWorld ();
            break;

        case Pause:
            boost::shared_ptr<StageMenuState> menu (
                    new StageMenuState (resources ()));
            menu->OnUndo.connect (boost::bind (&StageState::onActionBegan,
                        this, Undo));
            menu->OnReset.connect (boost::bind (&StageState::onActionBegan,
                        this, Reset));
            menu->OnQuit.connect (
                    boost::bind (&benzaiten::GameStateManager::removeActiveState,
                        &stateManager (), benzaiten::Fade::None));
            stateManager ().setActiveState (menu, benzaiten::Fade::None);
            break;
    }
}

void
StageState::onActionEnded (int action)
{
    if ( (MoveLeft == action && Left == direction ()) ||
         (MoveRight == action && Right == direction ()) )
    {
        direction_ = None;
    }
}

void
StageState::resetTimeToUpdate ()
{
    setTimeToUpdate (5);
}

const benzaiten::ResourceManager &
StageState::resources () const
{
    return resources_;
}

void
StageState::setTimeToUpdate (unsigned int time)
{
    timeToUpdate_ = time;
}

Stage &
StageState::stage ()
{
    return stage_;
}

void
StageState::stopActors ()
{
    view ().stopActor (Stage::BlackWorld,
            stage ().getActorDirection (Stage::BlackWorld));
    view ().stopActor (Stage::WhiteWorld,
            stage ().getActorDirection (Stage::WhiteWorld));
}

unsigned int
StageState::timeToUpdate () const
{
    return timeToUpdate_;
}

void
StageState::update (float elapsedTime)
{
    label_.update (elapsedTime);

    StageView::Offset offset = view ().getActorOffset (world ());
    if ( 0 != offset.first || 0 != offset.second )
    {
            view ().updateActorOffset (world ());
    }
    else
    {
        if ( stage ().getActorPosition (Stage::BlackWorld) ==
                stage ().getActorPosition (Stage::WhiteWorld) )
        {
            OnStageClear ();
            stateManager ().removeActiveState (benzaiten::Fade::None);
            stateManager ().setActiveState (
                    IGameState::ptr (new StageClearState (resources ())),
                                     benzaiten::Fade::Flash);
        }
        if ( stage ().isActorFalling (world ()) )
        {
            updateActorPosition (stage ().getActorDirection (world ()));
        }
        else if (None == direction () )
        {
            view ().stopActor (world (), stage ().getActorDirection (world ()));
            setTimeToUpdate (0);
        }
        else
        {
            if ( 0 == timeToUpdate () )
            {
                Stage::Direction nextActorDir =
                    (Left == direction () ? Stage::Left : Stage::Right );
                if ( nextActorDir != stage ().getActorDirection (world ()) )
                {
                    stage ().setActorDirection (world (), nextActorDir);
                    view ().stopActor (world (), nextActorDir);
                    resetTimeToUpdate ();
                }
                else
                {
                    updateActorPosition (nextActorDir);
                }
            }
            else
            {
                setTimeToUpdate (timeToUpdate () - 1);
            }
        }
    }
}

void
StageState::updateActorPosition (Stage::Direction toDirection)
{

    Stage::Position oldPos (stage ().getActorPosition (world ()));
    Stage::Position newPos (stage ().moveActor (world (), toDirection));

    if ( newPos == oldPos )
    {
        view ().stopActor (world (), toDirection);
    }
    else
    {
        if ( newPos.y > oldPos.y )
        {
            view ().fallActor (world (), stage ().getActorDirection (world ()));
        }
        else
        {
            view ().moveActor (world (), toDirection, newPos.y < oldPos.y);
        }
    }
}

StageView &
StageState::view ()
{
    return view_;
}

Stage::World
StageState::world () const
{
    return world_;
}

void
StageState::world (Stage::World world)
{
    world_ = world;
}
