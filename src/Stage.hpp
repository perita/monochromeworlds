//
// Monochrome Worlds - The White and Black Puzzle Game.
// Copyright (C) 2008, 2009, 2010 Geisha Studios
//
// This program is free software; you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation; either version 2 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program; if not, write to the Free Software
// Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
//
#if !defined (GEISHA_STUDIOS_MONOCHROME_WORLDS_STAGE_HPP)
#define GEISHA_STUDIOS_MONOCHROME_WORLDS_STAGE_HPP

#if defined (_MSC_VER) && (_MSC_VER >= 1020)
#pragma once
#endif // _MSC_VER && _MSC_VER >= 1020

#include <stack>
#include <stdexcept>
#include <string>
#include <boost/multi_array.hpp>
#include <boost/signals.hpp>
#include "Undo.hpp"

namespace monochromeworlds
{
    ///
    /// @class StageFileNotFoundError
    /// @brief Thrown when the stage file couldn't be opened.
    ///
    class StageFileNotFoundError: public std::invalid_argument
    {
        public:
            ///
            /// @brief Constructor.
            ///
            /// @param[in] fileName the file that couldn't be found.
            ///
            StageFileNotFoundError (const std::string &fileName):
                std::invalid_argument (
                        std::string ("couldn't find stage file: ") + fileName)
            {
            }
    };

    ///
    /// @class StageDataInvalidError
    /// @brief Thrown when the data in the stage's file is invalid.
    ///
    class StageDataInvalidError: public std::runtime_error
    {
        public:
            ///
            /// @brief Constructor.
            ///
            StageDataInvalidError ():
                std::runtime_error ("invalid stage data")
            {
            }
    };

    ///
    /// @class Stage
    /// @brief The model for a game's single stage.
    ///
    class Stage
    {
        public:
            ///
            /// @enum World
            /// @brief The two worlds.
            ///
            enum World
            {
                BlackWorld = 0,
                WhiteWorld = 1
            };

            ///
            /// @enum Direction
            /// @brief The direction that an actor can face.
            ///
            enum Direction
            {
                Left = -1,
                Right = 1
            };

            ///
            /// @struct Block
            /// @brief An stage's block.
            ///
            struct Block
            {
                ///
                /// @enum Attribute
                /// @brief A block's attribute.
                ///
                enum Attribute
                {
                    /// The block can be toggled from one color to the other.
                    Togglable,
                    /// The block can't be toggled from one color to the other.
                    Untogglable,
                    /// The block toggles automatically when an actor steps out.
                    Autotoggle
                };

                ///
                /// @enum Type
                /// @brief A block's type.
                ///
                enum Type
                {
                    Black = 0,
                    White = 1,
                    Wall = 2,
                    Goal = 3
                };


                /// The block's attribute.
                Attribute attribute;
                /// The block's type.
                Type type;

                ///
                /// @brief Initializes a block.
                ///
                /// @param[in] type The block's type.
                /// @param[in] attribute The block's attribute.
                ///
                explicit Block (Type type = Wall,
                        Attribute attribute = Untogglable):
                    attribute (attribute),
                    type (type)
                {
                }
            };

            ///
            /// @struct Position
            /// @brief A position in the stage.
            ///
            struct Position
            {
                /// The X coordinate of the position.
                int x;
                /// The Y coordinate of the position.
                int y;

                ///
                /// @brief Initializes a Position.
                ///
                /// @param[in] x The X coordinate to initialize.
                /// @param[in] y The Y coordinate to initialize.
                ///
                explicit Position (int x = 0, int y = 0):
                    x (x),
                    y (y)
                {
                }
            };

            ///
            /// @class Actor
            /// @param An actor.
            ///
            struct Actor
            {
                /// The actor's direction.
                Direction direction;
                /// The actor's position.
                Position position;
                /// The actor's start direction.
                Direction startDirection;
                /// The actor's start position.
                Position startPosition;
            };

            ///
            /// @class InvalidPositionError
            /// @brief Thrown when an passed coordinate is invalid.
            ///
            class InvalidPositionError: public std::invalid_argument
            {
                public:
                    ///
                    /// @brief Constructor.
                    ///
                    /// @param[in] message The error message.
                    ///
                    InvalidPositionError (const std::string &message):
                        std::invalid_argument (message)
                    {
                    }
            };

            /// A block is toggled except when undoing.
            mutable boost::signal<void (const Position &)> OnToggle;

            ///
            /// @brief Creates a new stage where all the blocks are walls.
            ///
            /// @param[in] width The width, in blocks, of the stage.  Must be
            ///            greater than 0.
            /// @param[in] height The height, in blocks, of the stage.  Must be
            ///            greater than 0.
            ///
            Stage (int width, int height);

            ///
            /// @brief Copy constructor.
            ///
            /// @param[in] stage The stage to copy from.
            ///
            Stage (const Stage &stage);

            ///
            /// @brief Gets the direction of an actor.
            ///
            /// @param[in] world The world of the actor to get its direction.
            ///
            /// @return The direction of actor in world @p world.
            ///
            Direction getActorDirection (World world) const;

            ///
            /// @brief Gets the position of an actor.
            ///
            /// @param[in] world The world of the actor to get its direction.
            ///
            /// @return The constant reference to the postition of actor in
            ///         world @p world.
            ///
            const Position &getActorPosition (World world) const;

            ///
            /// @brief Gets the stage's height.
            ///
            /// @return The stage's height in blocks.
            ///
            int height () const;

            ///
            /// @brief Tells if an actor is falling.
            ///
            /// @param[in] world The world of the actor to check if is falling.
            ///
            /// @return @c true if the below the actor in @p world there is
            ///         no solid block.
            ///
            bool isActorFalling (Stage::World world) const;

            ///
            /// @brief Loads an stage file.
            ///
            /// @param[in] fileName The file name of the stage to load.
            ///
            /// @return The stage in @p fileName already loaded.
            ///
            /// @throw StageFileNotFoundError if the stage's file \p fileName
            ///        couldn't be opened.
            /// @throw StageDataInvalidError if the file contains invalid data.
            ///
            static Stage load (const std::string &string);

            ///
            /// @brief Moves the actor to a direction, if it can.
            ///
            /// Moves the actor to next next possible block to \p direction.
            ///
            /// The movement is like this:
            ///
            ///  - If there is no solid block to the \p direction, it moves one
            ///    position in X to \p direction and as many positions down in Y
            ///    until the actor is on top of a solid block.
            ///  - If there is a solid block to \p direction, but on top of
            ///    that there is not solid block, then it moves one position
            ///    in X to direction and one in Y to up.
            ///
            /// @param[in] world The world of the actor to move.
            /// @param[in] direction The direction to move the actor to.
            ///
            /// @return The new position where the actor is after the move.
            ///
            Position moveActor (World world, Direction direction);

            ///
            /// @brief Gets the reference to a block.
            ///
            /// @param[in] x The X coordinate to get the block's reference from.
            /// @param[in] y The Y coordinate to get the block's reference from.
            ///
            /// @return The constant reference to the block at position @p x,
            ///         @p y or a reference to a wall block if @p x or @p y
            ///         are outside the stage's limits.
            ///
            const Block &operator() (int x, int y) const;

            ///
            /// @brief Resets the stage to its initial state.
            ///
            void reset ();

            ///
            /// @brief Sets the direction an actor is facing.
            ///
            /// @param[in] world The world of the actor to set her direction.
            /// @param[in] direction The direction that the actor must face.
            ///
            void setActorDirection (World world, Direction direction);

            ///
            /// @brief Sets the position of an actor.
            ///
            /// @param[in] world The world of the actor to set her position.
            /// @param[in] x The X coordinate to set the actor to.  It must be
            ///            between 0 and width() - 1.
            /// @param[in] y The Y coordinate to set the actor to.  It must be
            ///            between 0 and height() - 1.
            ///
            /// @throw InvalidPositionError if @p x or @p y are invalid, or
            ///        if the actor in world @p world can't be put in
            ///        the block at @p x, @p y.
            ///
            void setActorPosition (World world, int x, int y);

            ///
            /// @brief Sets the start position and direction of an actor.
            ///
            /// It also sets the current position and direction of that
            /// actor.
            ///
            /// @param[in] world The world of the actor to set her position
            ///            and direction.
            /// @param[in] x The X coordinate to set the actor to.  It
            ///              must be between 0 and width () - 1.
            /// @param[in] y The Y coordinate to set the actor to.  It must be
            ///            between 0 and height () - 1.
            /// @param[in] direction The direction that the actor must face.
            ///
            /// @throw InvalidPositionError if @p x or @p y are invalid, or
            ///        if the actor in world @p world can't be put in
            ///        the block at @p x, @p y.
            void setActorStartPositionAndDirection (World world, int x, int y,
                    Direction direction);

            ///
            /// @brief Sets the type and the attribute of a block.
            ///
            /// @param[in] x The X coordinate of the block to set.  It must
            ///            be between 0 and width() - 1.
            /// @param[in] y The Y coordinate of the block to set.  It must
            ///            be between 0 and height() - 1.
            /// @param[in] type The type to set the the block.
            /// @param[in] attribute The attribute to set to the block.  If
            ///            @p type is @c Wall or @c Goal,
            ///            this value is ignored and is set to @c Untogglable.
            ///
            /// @throw InvalidPositionError if @p x or @p y are invalid.
            ///
            void setBlock (int x, int y, Block::Type type,
                    Block::Attribute attribute);

            ///
            /// @brief Toggles a block, if possible.
            ///
            /// @param[in] x The X coorindate of the block to toggle.
            /// @param[in] y The Y coorindate of the block to toggle.
            /// @param[in] undoable Tells if the toggling is from undo.
            ///
            /// @return @c true if the block was toggled.
            ///
            bool toggle (int x, int y, bool undo = false);

            ///
            /// @brief Undoes the last action, if any.
            ///
            /// @return @c if an action has been undoed.
            ///
            bool undo ();

            ///
            /// @brief Gets the stage's width.
            ///
            /// @return The stage's width in blocks.
            ///
            int width () const;

        private:
            ///
            /// @brief Appends an undo command.
            ///
            /// @param[in] undo The undo command to append.
            ///
            void appendUndo (IUndo::ptr undo);

            ///
            /// @brief Gets the constant reference to an actor.
            ///
            /// @param[in] world The world of the actor to get.
            ///
            /// @return The constant reference to the actor whose world is
            ///         @p world.
            ///
            const Actor &getActor (World world) const;

            ///
            /// @brief Gets the reference to an actor.
            ///
            /// @param[in] world The world of the actor to get.
            ///
            /// @return The reference to the actor whose world is @p world.
            ///
            Actor &getActor (World world);

            ///
            /// @brief Gets the block at a position.
            ///
            /// @param[in] x The X position of the block to get.
            /// @param[in] y The Y position of the block to get.
            ///
            /// @return The reference of the block at @p x, @p y or
            ///         the reference to a wall block if @p x or @p y
            ///         are invalid.
            ///
            Block &getBlock (int x, int y);

            ///
            /// @brief Tells if a block is solid for a actor's color.
            ///
            /// @param[in] world The actor's world to check.
            /// @param[in] x The X coordinate of the block to check if
            ///            is solid.
            /// @param[in] y The Y coordinate of the block to check if
            ///            is solid.
            ///
            /// @return @c true if the block at @p x, @p y is solid for
            ///         the actor whose world is @p color.
            bool isSolidBlock (World world, int x, int y) const;

            ///
            /// @brief Toggles a block's type.
            ///
            /// @param[in] x The x position of the block to toggle.
            /// @param[in] y The y position of the block to toggle.
            /// @param[in] undo Tells if the toggle is from an undo.
            /// @param[inout] block The reference to the block to toggle.
            ///
            void toggleBlock (int x, int y, bool undo, Block &block);

            /// The actors.
            Actor actors_[2];
            /// The stage's blocks.
            boost::multi_array<Block, 2> blocks_;
            /// The undo stack.
            std::stack<IUndo::ptr> undo_;
            /// A wall block.
            Block wallBlock_;
    };

    ///
    /// @brief Checks if two positions are equal.
    ///
    /// @param[in] lhs The left hand side operator.
    /// @param[in] rhs The right hand side operator.
    ///
    /// @return @c true if @p lhs and @p rhs are equal.
    ///
    inline bool
    operator== (const Stage::Position &lhs, const Stage::Position &rhs)
    {
        return lhs.x == rhs.x && lhs.y == rhs.y;
    }

    ///
    /// @brief Checks if two positions are different.
    ///
    /// @param[in] lhs The left hand side operator.
    /// @param[in] rhs The right hand side operator.
    ///
    /// @return @c true if @p lhs and @p rhs are different.
    ///
    inline bool
    operator!= (const Stage::Position &lhs, const Stage::Position &rhs)
    {
        return lhs.x != rhs.x || lhs.y != rhs.y;
    }
}

#endif // !GEISHA_STUDIOS_MONOCHROME_WORLDS_STAGE_HPP
