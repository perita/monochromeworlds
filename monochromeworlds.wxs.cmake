<?xml version='1.0' encoding='latin1'?>
<Wix xmlns='http://schemas.microsoft.com/wix/2006/wi'
     xmlns:gaming='http://schemas.microsoft.com/wix/GamingExtension'
     xmlns:util='http://schemas.microsoft.com/wix/UtilExtension'>
  <Product Id='{DA9DC780-954C-11DE-8A39-0800200C9A66}' Name='Monochrome Worlds' Language='1033' Version='@VERSION@' Manufacturer='Geisha Studios' UpgradeCode='{748E6D97-A739-44CF-A6C4-39ADFFFC9399}'>
    <Package Description='The White and Black Puzzle Game' Manufacturer='Geisha Studios' InstallScope='perMachine' Keywords='white black puzzle game causal' InstallerVersion='200' Compressed='yes' Platform='x86'/>
    <Media Id='1' Cabinet='monochromworlds.cab' EmbedCab='yes'/>
    <Upgrade Id='{748E6D97-A739-44CF-A6C4-39ADFFFC9399}'>
      <UpgradeVersion Minimum='@VERSION@' IncludeMinimum='no' OnlyDetect='yes' Language='1033' Property='NEWPRODUCTFOUND'/>
      <UpgradeVersion Minimum='1.0.0.0' IncludeMinimum='yes' Maximum='@VERSION@' IncludeMaximum='no' Language='1033' Property='UPGRADEFOUND'/>
    </Upgrade>
    <Property Id='WIXUI_INSTALLDIR' Value='INSTALLFOLDER'/>
    <Property Id='WIXUI_EXITDIALOGOPTIONALCHECKBOX' Value='1' />
    <Property Id='WIXUI_EXITDIALOGOPTIONALCHECKBOXTEXT' Value='Launch Monochrome Worlds' />
    <Property Id='WixShellExecTarget' Value='[#monochromeworlds.exe]'/>
    <CustomAction Id='LaunchGame' BinaryKey='WixCA' DllEntry='WixShellExec' Impersonate='yes'/>
    <UI>
      <UIRef Id='Benzaiten_WixUI_InstallDir'/>
      <UIRef Id='WixUI_ErrorProgressText'/>
      <Publish Dialog='BenzaitenExitDialog' Control='Finish' Event='DoAction' Value='LaunchGame'>WIXUI_EXITDIALOGOPTIONALCHECKBOX = 1 and NOT Installed</Publish>
    </UI>
    <Icon Id='monochromeworlds.ico' SourceFile='@CMAKE_SOURCE_DIR@/data/gfx/monochromeworlds.ico'/>
    <Property Id='ARPPRODUCTICON' Value='monochromeworlds.ico' />

    <Directory Id='TARGETDIR' Name='SourceDir' DiskId='1'>
      <Directory Id='ProgramFilesFolder' Name='PFiles'>
        <Directory Id='GeishaStudiosFolder' Name='Geisha Studios'>
          <Directory Id='INSTALLFOLDER' Name='Monochrome Worlds'>
           <Component Id='MonochromeWorldsBinaryComponent' Guid='{15ACAF16-3878-4CD1-B232-12DCA3E8A673}'>
             <File Source='@CMAKE_BINARY_DIR@/src/monochromeworlds.exe' KeyPath='yes'>
               <gaming:Game Id='7B9E5F80-A4DC-11DD-AD8B-0800200C9A66'>
                 <gaming:PlayTask Name='Play!'/>
                 <gaming:SupportTask Name='Home Page' Address='http://www.geishastudios.com/games/monochromeworlds/'/>
               </gaming:Game>
             </File>
           </Component>
           <Component Id='SDLBinaryComponent' Guid='{0E3B407D-99A1-4EA7-9348-438D953342DA}'>
             <File Source='@CMAKE_SOURCE_DIR@/lib/benzaiten/3rdparty/SDL/SDL.dll' KeyPath='yes'/>
           </Component>
           <Component Id='SDLmixerBinaryComponent' Guid='{2BEEC243-F38B-449C-8AA3-7A5D770493F6}'>
             <File Source='@CMAKE_SOURCE_DIR@/lib/benzaiten/3rdparty/SDL_mixer/SDL_mixer.dll' KeyPath='yes'/>
             <File Source='@CMAKE_SOURCE_DIR@/lib/benzaiten/3rdparty/SDL_mixer/ogg.dll'/>
             <File Source='@CMAKE_SOURCE_DIR@/lib/benzaiten/3rdparty/SDL_mixer/vorbis.dll'/>
             <File Source='@CMAKE_SOURCE_DIR@/lib/benzaiten/3rdparty/SDL_mixer/vorbisfile.dll'/>
           </Component>
           <Directory Id='GraphicsFolder' Name='gfx'>
             <Component Id='MonochromeWorldsGraphicsComponent' Guid='{E0784E3D-026B-42C6-A7F3-4F2E16508BFC}'>
               <?foreach GRAPHIC in @GFX_FILES@?>
               <File Source='$(var.GRAPHIC)'/>
               <?endforeach?>
             </Component>
           </Directory>
           <Directory Id='StagesFolder' Name='stages'>
             <Component Id='MonochromeWorldsStagesComponent' Guid='{CD104E9C-973C-425D-896D-4A6149C3FEAD}'>
               <?foreach STAGE in @STAGE_FILES@?>
               <File Source='$(var.STAGE)'/>
               <?endforeach?>
             </Component>
           </Directory>
           <Directory Id='MusicFolder' Name='music'>
             <Component Id='MonochromeWorldsMusicComponent' Guid='{32BF1B56-D90E-427A-A312-8D2F996A8CC2}'>
               <?foreach MUSIC in @MUSIC_FILES@?>
               <File Source='$(var.MUSIC)'/>
               <?endforeach?>
             </Component>
           </Directory>
           <Directory Id='SoundsFolder' Name='sfx'>
             <Component Id='MonochromeWorldsSoundsComponent' Guid='{3DF7D89D-344D-4768-9F81-E26079286489}'>
               <?foreach SFX in @SFX_FILES@?>
               <File Source='$(var.SFX)'/>
               <?endforeach?>
             </Component>
           </Directory>
          </Directory>
        </Directory>
      </Directory>
      <Directory Id='ProgramMenuFolder' Name='PMenu'>
        <Directory Id='MonochromeWorldsProgramMenuFolder' Name='Monochrome Worlds'>
          <Component Id='MonochromeWorldsStartMenuShortcutComponent' Guid='{83F4F414-6161-405C-9D19-2B379E6862BB}'>
            <Shortcut Id='MonochromeWorldsStartMenuShortcut' Name='Monochrome Worlds' Description='The White and Black Puzzle Game' Target='[INSTALLFOLDER]monochromeworlds.exe' WorkingDirectory='INSTALLFOLDER'/>
            <util:InternetShortcut Id='MonochromeWorldsHomeShortcut' Directory='MonochromeWorldsProgramMenuFolder' Name='Monochrome Worlds&apos; Home Page' Target='http://www.geishastudios.com/games/monochromeworlds/'/>
            <RemoveFolder Id='MonochromeWorldsProgramMenuFolder' On='uninstall'/>
            <RegistryValue Root='HKCU' Key='Software\Geisha Studios\Monochrome Worlds' Name='StartMenuShortcut' Type='integer' Value='1' KeyPath='yes'/>
          </Component>
        </Directory>
      </Directory>
    </Directory>

    <CustomAction Id='PreventDowngrading' Error='There is a newer version already installed'/>

    <InstallExecuteSequence>
      <Custom Action='PreventDowngrading' After='FindRelatedProducts'>NEWPRODUCTFOUND</Custom>
      <RemoveExistingProducts After='InstallFinalize'/>
    </InstallExecuteSequence>

    <InstallUISequence>
      <Custom Action='PreventDowngrading' After='FindRelatedProducts'>NEWPRODUCTFOUND</Custom>
    </InstallUISequence>

    <Feature Id='MonochromeWorlds' Title='Monochrome Worlds' Level='1' Display='expand' AllowAdvertise='no'>
      <ComponentRef Id='MonochromeWorldsBinaryComponent'/>
      <ComponentRef Id='MonochromeWorldsGraphicsComponent'/>
      <ComponentRef Id='MonochromeWorldsStartMenuShortcutComponent'/>
      <ComponentRef Id='MonochromeWorldsStagesComponent'/>
      <ComponentRef Id='MonochromeWorldsMusicComponent'/>
      <ComponentRef Id='MonochromeWorldsSoundsComponent'/>
      <ComponentRef Id='SDLBinaryComponent'/>
      <ComponentRef Id='SDLmixerBinaryComponent'/>
    </Feature>
  </Product>
</Wix>
